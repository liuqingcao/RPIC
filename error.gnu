	
	set terminal postscript eps color solid

	set xtics nomirror
	set ytics nomirror
	unset x2tics
	set y2tics

	set xtics font "times.ttf,20"
	set ytics font "times.ttf,20"
	set y2tics font "times.ttf,20"

	set xlabel 'Step' font "times.ttf,20"
	set ylabel 'Error' font "times.ttf,20"
	set y2label 'Out Number' font "times.ttf,20"


	set ytics  tc lt 1
	set y2tics  tc lt 3
	
	set ylabel  tc lt 1
	set y2label  tc lt 3

	set key left
	set xrange[0:]
	set yrange[-100:100]
	
	set output 'x1y1.eps'
	plot "./error.txt" u 1:2 w p lc 1 , "./error.txt" u 1:3 w l lw 3 lc 3 axes x1y2

