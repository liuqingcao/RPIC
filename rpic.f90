!************************rpic.f90***************************************
!说明：在整个程序，如果在当前程序中没有找到定义的就一定是在头文件中定义的，并已定义为
!	全局变量，这是为了在整个程序中的使用方便，减少了程序之间的数据传递，但会增加程序
!	运行的内存。在看程序是，如果不知道这些变量里储存的是什么信息，可以使用复制这个变量
!	然后打开rcommon.h文件中按下Ctrl+F（查找）、Ctrl+V、<下一个>，就可以找到定义的
!	地方和一些简单的参量说明。
!	程序分成了两个主体部分，初始化部分和主体循环部分。
!	每个子程序前面都会有详细的说明，包括子程序的思路、到达的目的和需要注意的事情。
!	如果使用Geany打开本文件，可以在左侧标记显示的空白处点鼠标右键，选择<按外观排序>
!	这样显示的子程序是按照在程序中出现的顺序排序，便于查看。
!	其实这样写程序的习惯是不好的，就是把所有程序都放到一个文件里，比较好的方式是
!	把所有的子程序都写到不同的文件里，而且各有一个对应的头文件，头文件里定义变量和
!	全局变量，这样便于阅读和修改，而且在扩展和调试上也比较方便，在程序中用include
!	的方式把需要共享数据的子程序结合起来,再使用写makefile文件的方式来编译各个部
!	分，并执行。
!***********************************************************************

program main
	implicit none
	include 'rcommon.h'
	integer::showtime=10,pipetemp,i,j
	logical::moveflag=.true.
	real*8 cput11,cput22,cput23,cput34,cput45
	!************************程序初始化部分********************************
	!说明：初始化靶和场的一些数据，计算一些公式中经常成
	!获取CPU时间，当做程序运行起始时间，cput0在头文件中定义，是全局变量
	call cpu_time(cput0)
	!初始化量计算
	call init_comput()
	!初始化求解MAXWEL方程的一些系数
	call init_maxwel()
	!把一些参量置0
	call setzero()
	!设置靶-1，形状，梯度，速度，位置等
	call target1()
	!call hand_hole(cnumi1,xyi1,iconi1,tgtmx1)
	!call hand_hole(cnume1,xye1,icone1,tgtmx1)

	!设置靶-2
	call target2()
	!call hand_hole(cnumi2,xyi2,iconi2,tgtmx2)
	!call hand_hole(cnume2,xye2,icone2,tgtmx2)

	! 用来判断改变靶的形状是否正确
	call shapecheck()
	!参量输出，简单计算并输出一些可以直观看到的计算参量，如计算时间，粒子个数等
	call table()
	call cpu_time(cput1)
	write(*,1000)(cput1-cput0)
	1000 FORMAT("初始化时间：",F6.2," S")
	!初始化求解poisson方程,得到初始时刻的半格点上的电场
	if(pois_init==1.and.(tgt1+tgt2)>0)then
		call init_poisson()
		call fix_poisson()
		call bz(0.5d0,0.5d0)
		step=-1
		call error()
	end if
	write(*,*)" 进度  单步时间     剩余时间   传输时间"

	!*************************主体循环部分*********************************
	!说明：此部分是程序主体循环的部分，以时间步长的个数为循环个数，分别按顺序计算了：输出
	!	参量->计算边界上的激光场（源场）->格点上的场分配到粒子上->粒子推动（粒子边界条
	!	件）->粒子电流密度分配到格点->求解MAXWEL(场边界条件)->把半格点上的场平均到整
	!	格点 ; 按这样的顺序循环计算。
	!	具体思路见每个子程序的说明。
	!以时间步数开始循环

	do step=0,totstep
		rtime=step*dt  !模拟的时间=时间步数*时间步长
		call cpu_time(cput2)
		call check() !定时输出一些参量，主要的数据输出部分
		call cpu_time(cput3)
		cput23=(cput3-cput2)
		pipetemp=pipe
		pipe=step*100/totstep
		if(pipe>0.and.mod(pipe,showtime)==0.and.pipe>pipetemp)then
			!计算剩余时间，用单个循环时间*剩余的步数，单位：分钟
			cput22=cput11*(totstep-step)/60.
			!往屏幕上格式化输出一些信息
			write(*,100)pipe,cput11,cput22,rtime/per
			100 FORMAT(I3," %",3x,F5.2," s",3x,F6.1," min",3x,F6.2," T")
			write(*,200)cki,cput23,cput34,cput45
			200 FORMAT(3x,I3,1x,"Files",3x,"Output:",F5.2,"s",3x,"Particle:",F5.2,"s",3x,"Field:",F5.2,"s")
			if(tgt1==1.and.rtime>=movetime*per)then
				write(*,300)cputp12,cputp23,cputp34,cputp45
				300 FORMAT(4x,"tgt1:i_move:",F5.2,"s",3x,"i_curr:",F5.2,"s",3x,"e_move:",F5.2,"s",3x,"e_curr:",F5.2,"s")
			end if
			if(tgt2==1.and.rtime>=movetime*per)then
				write(*,400)cputp67,cputp78,cputp89,cputp90
				400 FORMAT(4x,"tgt2:i_move:",F5.2,"s",3x,"i_curr:",F5.2,"s",3x,"e_move:",F5.2,"s",3x,"e_curr:",F5.2,"s")
			end if
		end if

		if(rtime>=movetime*per.and.tgt1+tgt2>0)then
			if(moveflag)then
				write(*,*)"----------Particle Move On----------"
				moveflag=.false.
			end if
			call average() !把半格点上的场平均到整格点
			call particle() !粒子运动部分

		end if

		call cpu_time(cput4)
		cput34=(cput4-cput3)


		call maxwel() !场的传播
		call error()


		call energy()



		call laser()  !激光源


		!粒子运动后,间隔指定步长进行一次修正
		if(rtime>=movetime*per.and.pois_onf==1.and.(tgt1+tgt2)>0)then
			if(mod(step,pois_fix)==0)then
				call fix_poisson()
				call error()
			end if
		end if

		call cpu_time(cput5)
		cput45=(cput5-cput4)
		cput11=(cput5-cput3)
	end do



	!获取现在时间，计算程序运行的总时间并输出，单位分钟
	call cpu_time(cput10)
	write(*,20000)(cput10-cput0)/60.	!这里的20000是格式化输出的方法
	20000 FORMAT("计算总时间：",F8.2,"  Min")

stop
end program

subroutine hand_hole(cnum,xy,icon,tgtmx)
	implicit none
	include 'rcommon.h'
	integer cnum,icon(cnum),i
	real*8 xy(3,cnum),x,y,tgtmx
	real*8::depth=0.08d-6
	real*8::ylmd=0.5d-6
	do i=1,cnum
		x=xy(1,i)*1d6
		y=xy(2,i)*1d6
		if(x<10.05-0.05*exp(-1d0*((y-16.)/7.)**2))then
			icon(i)=0
		end if
	end do
return
end subroutine



!说明：此子程序以前是用来计算一些初始化量得，后来把这个简单的初始化计算放到了头文
!	件中完成，如rcommon.h文件中第30行到第36行的计算。现在是作一些简单的程序测试用
subroutine init_comput()
	implicit none
	include 'rcommon.h'
	integer tgtsame

	laserl=5!
	laserr=xnum-6

	!电场量纲转换
	fdtrans=-1d0*q/(me*co*womiga)
	!
	fdtransb=fdtrans*co
	!计算总时间
	totime=totimet*per
	!计算计算总时间步数
	totstep=totime/dt

	!检查记录时间设置是否正确
	if(btime<0d0.or.btime>=totimet)then
		write(*,*)"开始记录数据时间设置错误!"
		stop
	end if
	if(ftime>totimet.or.btime>=ftime)then
		write(*,*)"停止记录数据时间设置错误"
		stop
	end if
	!需要记录多少步数
	rcdsteps=(ftime-btime)*per/dt
	!开始和结束记录的步数
	bstep=btime*per/dt
	fstep=ftime*per/dt

	!靶2和靶1材料是否相同
	if(relam2==relam1)then
		tgtsame=1
	else
		tgtsame=0
	end if
	!输出一些计算的信息，数据处理用
	open(111,file='pipe')
	write(111,1111)tgt1,tgt2,tgtin,tgtsame,lx*1e6,ly*1e6
	1111 Format(I1,5x,I1,5x,I1,5x,I1,5x,F6.2,5x,F6.2)
	close(111)
return
end subroutine

!说明：计算出在求解MAXWEL方程时用到的一些经常出现的组合表达式，计算这些量时用到的
!	全部是常量或者是定义好的量，值是不会随着程序的运行发生变化的，所以放在程序的
!	开始就可以计算出这些量,可以很容易的把下面的表达式写成物理公式，再代入到
!	MAXWEL（）子程序的公式中，就可以写出完整的求解MAXWEL的公式了
subroutine init_maxwel()
	implicit none
	include 'rcommon.h'
	tpx=dt/dx
	tpy=dt/dy
	coo=co**2
	det=dt/ebsn
	tpex=tpx*coo
	tpey=tpy*coo

	ctx1=0.5d0*ctx+1d0
	ctx2=0.5d0*ctx-1d0
	cty1=0.5d0*cty+1d0
	cty2=0.5d0*cty-1d0

	fparx=(ctx-4d0)/(ctx+4d0)
	fpary=(cty-4d0)/(cty+4d0)
	hparx=(ctx-2d0)/(ctx+2d0)
	hpary=(cty-2d0)/(cty+2d0)

	dxy=sqrt(dx**2+dy**2)
	jd1=0.5d0*co*dt/dxy+1d0
	jd2=0.5d0*co*dt/dxy-1d0
	jd3=(co*dt/dxy-4d0)/(co*dt/dxy+4d0)
	jd4=(co*dt/dxy-2d0)/(co*dt/dxy+2d0)
return
end subroutine


!说明：此子程序把一些定义的变量归零，需要归零是因为有些量如果在程序里没赋值而直接
!	参与到计算中时其值不一定为0，尤其是在数组中，容易出现这种问题。
subroutine setzero()
	implicit none
	include 'rcommon.h'
	!把半格点电磁场数组归零，用 =0. 是因为这些数组是定义成real的，用 =0 也可以，
	!编译器会自动转换的
	fdex=0d0
	fdey=0d0
	fdbz=0d0
	fdbx=0d0
	fdby=0d0
	fdez=0d0
	!把整格点上的电磁场数组归零
	fdex1=0d0
	fdey1=0d0
	fdbz1=0d0
	fdbx1=0d0
	fdby1=0d0
	!把粒子出界标识数组归1，iconi1就是icon(标识)+i(离子)+1（靶1），代表的是靶1的
	!各个离子是否出界，在粒子运动子程序中的粒子边界条件部分会进行判断，若出界了，
	!则赋值0，注意，这是一个数组，数组的大小就对应各种计算粒子的个数
	iconi1=1
	icone1=1
	iconi2=1
	icone2=1
	!粒子对格点电流贡献归零，cutxe1就是current（电流）+ x（X方向）+ e(电子) + 1
	!（靶1），这个数组代表的就是靶1中的电子对格点的x方向的电流贡献，在粒子电流往
	!格点上分配的时候，各种不同的粒子都是分开计算的，存储在各自的数组中，最后再在
	!三个方向中分别相加，得到cutx，cuty，cutz，这三个数组里的量就是在求解MAXWEL
	!的公式里需要用到的jx，jy，jz。
	cutx=0d0
	cuty=0d0
	cutz=0d0
	row=0d0
	cutxi1=0d0
	cutyi1=0d0
	cutzi1=0d0
	rowi1=0d0
	cutxe1=0d0
	cutye1=0d0
	cutze1=0d0
	rowe1=0d0
	cutxi2=0d0
	cutyi2=0d0
	cutzi2=0d0
	rowi2=0d0
	cutxe2=0d0
	cutye2=0d0
	cutze2=0d0
	rowe2=0d0
	!激光功率密度和振幅数组
	laserao=0d0
	!总能量
	totfield=0d0
	totpart=0d0
	totlaser=0d0
	lari1=0d0
	lari2=0d0
	cki=0

	poutnum=0
return
end subroutine


!说明：初始化靶1的所有信息
subroutine target1()
	implicit none
	include 'rcommon.h'
	integer ij(xnum+1,ynum+1),i,j,sumij
	real*8 wti,wte,x,y,rnumi,rnume
	!判断是否计算靶1，如果tgt1==0就直接返回了，不进行下面的计算了
	if(tgt1==0)then
		return
	end if
	idens1=edens1/dble(exte1)  !靶1的离子密度=靶1电子密度/价态（电子个数）
	mi1=relam1*mc12    !靶1实际离子质量=相对原子质量*碳12的实际质量
	qi1=abs(q*dble(exte1))     !靶1实际离子电荷数=（电子电荷数*价态）的绝对值
	rnumi=idens1*s       !每个格子里实际分配的离子个数=粒子密度*单个格子的面积
	rnume=edens1*s       !每个格子里实际分配的电子个数=电子密度*单个格子的面积
	wti=rnumi/dble(celli1)	!靶1模拟离子的权重=每格实际离子个数/头文件里设置的
							!每格模拟离子的个数，real(celli1)是把整数转换成
							!浮点数，统一计算的数据类型
	wti1=wti
	wte=rnume/dble(celle1)  !靶1模拟电子的权重
	wte1=wte
	call hole(ij,tgtmx1,tgtmy1,tgtx1,tgty1,honum1,1)
	!靶1挖孔,调用挖孔子程序
	!传递的参数有:
	!	ij：整型数组，返回的是整个计算区域哪个格子里应该分配靶1粒子，比如（100，200）
	!		这个格点要分配粒子，则ij（100，200）就要等于1，其它没有的就都为0
	!	tgtmx1:靶1的左边界中点的横坐标，tgtmx1 = target + middle + X +1
	!	tgtmy1:靶1的左边界中点的纵坐标
	!	tgtx1:靶1的宽度
	!	tgty1:靶1的高度
	!	honum1:靶1的要挖的孔的个数，honum1 = hole + number + 1
	!	1:代表靶1，告诉子程序，现在需要处理得是靶1
	sumij=sum(ij)	!把返回来的ij数组求和，sumij就代表了靶1一共占了多少个格子
	cnumi1=sumij*dble(celli1) !靶1的离子的模拟个数=占的格子数*每格放置的模拟离子个数
	cnume1=sumij*dble(celle1) !靶1的电子的模拟个数=占的格子数*每格放置的模拟电子个数
	open(222,file='temp1')
	!新建一个名为temp1的文件，此部分的用处见auto_rpic.sh文件中的说明
	write(222,*)"snumi1=",cnumi1	!在文件第一行写入snumi1=靶1计算离子的个数
	write(222,*)"snume1=",cnume1	!在文件第二行写入snume1=靶1计算电子的个数
	!判断头文件中设置的靶1的粒子计算个数是否正确
	if(snumi1==cnumi1.and.snume1==cnume1)then
		write(222,*)"right"	!若正确，则在文件第三行写入right
	else
		write(222,*)"wrong"	!若不正确，则在文件第三行写入wrong
		stop				!把这个程序终止，终止后会继续执行auto_rpic.sh的命令
	end if
	!到有靶1粒子的所有格子里随机分配粒子，并计算模拟粒子的坐标、质量、电荷
	call init_xy(celli1,cnumi1,wti,mi1,qi1,ij,xyi1,cmi1,cqi1,piji1,disti1,weiti1,tgtmx1,tgtx1)
	call init_xy(celle1,cnume1,wte,me, q,  ij,xye1,cme1,cqe1,pije1,diste1,weite1,tgtmx1,tgtx1)
	!调用子程序，传递的参数有：
	!	celli1:靶1每格放置的离子个数
	!	cnumi1:靶1总的模拟离子个数
	!	wti:靶1模拟离子的权重
	!	mi1:靶1实际离子质量
	!	qi1:靶1实际离子电量
	!	ij:靶1要占哪些格子的
	!	xyi1:靶1模拟离子的坐标
	!	cmi1:靶1模拟离子的质量
	!	cqi1:靶1模拟离子的电量
	!	piji1:靶1模拟离子属于哪个格子
	!	weiti1:靶1每个离子的权重因子，其值>0 <=1，若靶没有密度梯度的时候则都为1,
	!			当有密度梯度的时候，靶前表面（或后表面）的格子里模拟粒子的个数不变
	!			但是这些模拟离子的质量和电荷都会乘上自己位置所对应一个权重因子，
	!			这个权重因子和离子所在位置和密度梯度的形状有关。
	!	tgtmx1:靶1的左边界中点的横坐标
	!	tgtx1：靶1的宽度

	!计算靶1粒子的初始化速度，速率，动量，相对论因子，能量
	call init_v(cnumi1,vxyi1,pxyi1,gamai1,cmi1,mi1,initi1,engi1)
	call init_v(cnume1,vxye1,pxye1,gamae1,cme1,me, inite1,enge1)

	!cnum,vxy,pxy,gama,cm,rm,init,eng
	!调用子程序，传递的参数有：
	!	cnumi1:靶1总的模拟离子个数
	!	vxyi1:靶1模拟离子的速度V，Vx，Vy，Vz
	!	pxyi1:靶1模拟离子的动量P，Px，Py，Pz
	!	gamai1:靶1模拟离子的相对论因子
	!	cmi1:靶1模拟离子的质量
	!	mi1:靶1模拟离子的电量
	!	initi1:靶1模拟离子的初始化温度
	!	engi1:靶1模拟离子的能量

	!靶1真实粒子个数
	realnum1i=wti*sum(weiti1)	!靶1真实离子个数=模拟离子的权重*靶1所有离子的
								!权重的求和
	realnum1e=wte*sum(weite1)	!靶1真实电子个数=模拟电子的权重*靶1所有电子的
								!权重的求和
return
end subroutine



!说明：初始化靶2的所有信息
subroutine target2()
	implicit none
	include 'rcommon.h'
	integer ij(xnum+1,ynum+1),i,j,sumij
	real*8 wti,wte,x,y,rnumi,rnume
	if(tgt2==0)then
		return
	end if
	idens2=edens2/dble(exte2)
	mi2=relam2*mc12
	qi2=abs(q*dble(exte2))
	rnumi=idens2*s
	rnume=edens2*s
	wti=rnumi/dble(celli2)
	wti2=wti
	wte=rnume/dble(celle2)
	wte2=wte
	call hole(ij,tgtmx2,tgtmy2,tgtx2,tgty2,honum2,2)
	sumij=sum(ij)
	cnumi2=sumij*dble(celli2)
	cnume2=sumij*dble(celle2)
	open(333,file='temp2')
	write(333,*)"snumi2=",cnumi2
	write(333,*)"snume2=",cnume2
	if(snumi2==cnumi2.and.snume2==cnume2)then
		write(333,*)"right"
	else
		write(333,*)"wrong"
		stop
	end if
	call init_xy(celli2,cnumi2,wti,mi2,qi2,ij,xyi2,cmi2,cqi2,piji2,disti2,weiti2,tgtmx2,tgtx2)
	call init_xy(celle2,cnume2,wte,me, q,  ij,xye2,cme2,cqe2,pije2,diste2,weite2,tgtmx2,tgtx2)
	call init_v(cnumi2,vxyi2,pxyi2,gamai2,cmi2,mi2,initi2,engi2)
	call init_v(cnume2,vxye2,pxye2,gamae2,cme2,me,inite2,enge2)
	realnum2i=wti*sum(weiti2)
	realnum2e=wte*sum(weite2)
return
end subroutine

!说明：初始化入射粒子的所有信息，暂时没用
subroutine inject()
	implicit none
	include 'rcommon.h'

	if(tgtin==0)then
		return
	end if






return
end subroutine




! 用来判断改变靶的形状是否正确
subroutine shapecheck()
	implicit none
	include 'rcommon.h'
	integer i
	if(tgt1==1)then
		open(10,file='tgt1.txt',status='REPLACE')
		do i=1,cnume1
			if(icone1(i)==1)write(10,*)xye1(1,i)*1d6,xye1(2,i)*1d6,weite1(i)
		end do
		close(10)
	end if
	if(tgt2==1)then
		open(11,file='tgt2.txt',status='REPLACE')
		do i=1,cnume2
			if(icone2(i)==1)write(11,*)xye2(1,i)*1d6,xye2(2,i)*1d6,weite2(i)
		end do
		close(11)
	end if

	if(tgtin==1)then

	end if
return
end subroutine


!solve the poisson and E
subroutine init_poisson()
	implicit none
	include 'rcommon.h'
	integer j,k,m,j1
	integer FFTL,errl
	real*8 fftinr(ynum),fftini(ynum)
	real*8 fftoutr(ynum),fftouti(ynum)
	integer,parameter::xnum1=xnum+1,ynum1=ynum+1,trde=3*xnum1-2
	real*8 rowr(xnum1,ynum),rowi(xnum1,ynum),rowtr(xnum1),rowti(xnum1)
	real*8 linea(trde),lineain(trde),fair(xnum1,ynum1),faii(xnum1,ynum1)
	real*8 dm,rvalue,rmax,rmin,tempp,tempt
	real*8 gtime0,gtime1
	call cpu_time(gtime0)
	rowr=0d0
	rowi=0d0
	rowtr=0d0
	rowti=0d0
	!求格点上的电荷密度
	call sum_row()
	!电荷密度在Y方向进行FFT变换
	do j=1,xnum1
		fftinr(1:ynum)=row(j,1:ynum)*dy
		fftini=0d0
		FFTL=0
		call FFT(fftinr,fftini,ynum,rowr(j,1:ynum),rowi(j,1:ynum),FFTL)
	end do
	!P(j,m)
	tempp=dx**2/ebsn
	rowr=rowr*tempp
	rowi=rowi*tempp
	!当左右为开放边界时
	if(pois_type==1)then
		!当dm=1(m=1,最下面一行)时，有解析解
		do j=1,xnum1
			rowtr(j)=0d0
			rowti(j)=0d0
			do j1=1,xnum1
				rowtr(j)=rowtr(j)-0.5d0*(dble(abs(j1-j))*rowr(j1,1))
				rowti(j)=rowti(j)-0.5d0*(dble(abs(j1-j))*rowi(j1,1))
			end do
		end do
		rowr(1:xnum1,1)=rowtr(1:xnum1)
		rowi(1:xnum1,1)=rowti(1:xnum1)
		!其他行
		do m=2,ynum
			dm=1d0+2d0*(dx/dy*sin(pi*(m-1)/ynum))**2
			rvalue=sqrt(dm*dm-1d0)
			rmax=dm+rvalue
			rmin=dm-rvalue
			linea=0d0
			linea(1)=rmax
			linea(2)=-1d0
			linea(trde-1)=-1d0
			linea(trde)=rmax
			do j=3,trde-4,3
				linea(j)=-1d0
				linea(j+1)=2d0*dm
				linea(j+2)=-1d0
			end do
			lineain=linea
			call ATRDE(lineain,xnum1,trde,rowr(1:xnum1,m),errl)
			call ATRDE(linea  ,xnum1,trde,rowi(1:xnum1,m),errl)
		end do
	end if

	!当左右边界电势为0时
	if(pois_type==0)then
		do m=1,ynum
			dm=1d0+2d0*(dx/dy*sin(pi*(m-1)/ynum))**2
			linea=0d0
			linea(1)=2d0
			linea(2)=-1d0
			linea(trde-1)=-1d0
			linea(trde)=2d0
			do j=3,trde-4,3
				linea(j)=-1d0
				linea(j+1)=2d0*dm
				linea(j+2)=-1d0
			end do
			lineain=linea
			call ATRDE(lineain,xnum1,trde,rowr(1:xnum1,m),errl)
			call ATRDE(linea  ,xnum1,trde,rowi(1:xnum1,m),errl)
		end do
	end if

	!对电势进行逆FFT变换
	do j=1,xnum1
		fftinr(1:ynum)=rowr(j,1:ynum)
		fftini(1:ynum)=rowi(j,1:ynum)
		fftoutr=0d0
		fftouti=0d0
		FFTL=1
		call FFT(fftinr,fftini,ynum,fftoutr,fftouti,FFTL)
		fair(j,1:ynum)=fftoutr(1:ynum)/dy
		fair(j,ynum1)=fair(j,1)
		faii(j,1:ynum)=fftouti(1:ynum)/dy
		faii(j,ynum1)=faii(j,1)
	end do
	!得到半格点上的电场
	do j=1,xnum
		do k=1,ynum1
			fdex(j,k)=(fair(j,k)-fair(j+1,k))/dx
		end do
	end do
	do j=1,xnum1
		do k=1,ynum
			fdey(j,k)=(fair(j,k)-fair(j,k+1))/dy
		end do
	end do
	!output
	!输出初始的电势分布
	open(11,file='init_fai.txt')
	do j=1,xnum1
		do k=1,ynum1
			write(11,*)j*dx*1e6,k*dy*1e6,fair(j,k),faii(j,k)
		end do
			write(11,*)
	end do
	close(11)
	!输出初始的电场分布
	open(10,file='init_exy.txt')
	do j=1,xnum
		do k=1,ynum
			write(10,*)j*dx*1e6,k*dy*1e6,fdex(j,k),fdey(j,k),sqrt(fdex(j,k)**2+fdey(j,k)**2)
		end do
			write(10,*)
	end do
	close(10)
	call cpu_time(gtime1)
	write(*,1001)gtime1-gtime0
	1001 FORMAT("初始化求解poisson时间：",F6.2," S")
return
end subroutine

!
subroutine sum_row()
	implicit none
	include 'rcommon.h'
	!get row
	if(tgt1==1)then
		call get_row(cqi1,cnumi1,piji1,disti1,rowi1,iconi1)
		call get_row(cqe1,cnume1,pije1,diste1,rowe1,icone1)
	end if
	if(tgt2==1)then
		call get_row(cqi2,cnumi2,piji2,disti2,rowi2,iconi2)
		call get_row(cqe2,cnume2,pije2,diste2,rowe2,icone2)
	end if
	!sum ROW
	row=rowi1+rowe1+rowi2+rowe2
	if(udbd==0)then
		!Y方向施加周期性条件，即把最上面的行和最下面的行重合起来
		row(:,1)=row(:,1)+row(:,ynum+1)
		row(:,ynum+1)=row(:,1)
	end if
return
end subroutine

!采用面积权重的方式分配粒子row
subroutine get_row(qa,cnum,pij,dist,rowo,icon)
	implicit none
	include 'rcommon.h'
	integer pii,pjj
	integer cnum,pij(3,cnum),it,icon(cnum)
	real*8 nq
	real*8 weight(4)
	real*8 rowo(xnum+1,ynum+1)
	real*8 qa(cnum),dist(2,cnum)
	rowo=0.0
	do it=1,cnum
		!若出界标识为真（没出界）
		if(icon(it)==1)then
			pii=pij(1,it)
			pjj=pij(2,it)
			!计算单个粒子的电流密度
			nq=qa(it)/s
			!权重
		!	write(*,*)nq
			call weighting(dist(:,it),weight)
			!row
			rowo(pii,pjj)=rowo(pii,pjj)+nq*weight(1)
			rowo(pii+1,pjj)=rowo(pii+1,pjj)+nq*weight(2)
			rowo(pii+1,pjj+1)=rowo(pii+1,pjj+1)+nq*weight(3)
			rowo(pii,pjj+1)=rowo(pii,pjj+1)+nq*weight(4)
		end if
	end do
return
end subroutine

!权重
subroutine weighting(dist,weight)
	implicit none
	include 'rcommon.h'
	real*8 dist(2),weight(4)
	real*8 wx,wy
	integer i,j
	!归零
	weight=0
	!从数组中取出粒子与左边界和下边界的距离，以dx和dy为单位
	wx=dist(1)
	wy=dist(2)
	!1阶，只用计算四块面积，按逆时针方向旋转计算
	!右上
	weight(3)=wx*wy
	!右下
	weight(2)=wx*(1d0-wy)
	!左上
	weight(4)=(1d0-wx)*wy
	!左下
	weight(1)=1d0-weight(4)-weight(2)-weight(3)

return
end subroutine

SUBROUTINE FFT(inr,ini,n,outr,outi,L)
	implicit none
	include 'rcommon.h'
	integer l,n,i,j
	real*8 inr(n),ini(n),outr(n),outi(n)
	real*8 temp,temp1,temp2
	temp=2d0*pi/dble(n)
	outr=0d0
	outi=0d0
	if(l==0)then
		do i=0,n-1
			do j=0,n-1
				outr(i+1)=outr(i+1)+inr(j+1)*cos(i*j*temp)
				outi(i+1)=outi(i+1)-inr(j+1)*sin(i*j*temp)
			end do
		end do
	end if
	if(l==1)then
		do i=0,n-1
			do j=0,n-1
				temp1=cos(i*j*temp)
				temp2=sin(i*j*temp)
				outr(i+1)=outr(i+1)+inr(j+1)*temp1-ini(j+1)*temp2
				outi(i+1)=outi(i+1)+inr(j+1)*temp2+ini(j+1)*temp1
			end do
		end do
		outr=outr/dble(n)
		outi=outi/dble(n)
	end if
return
end subroutine


SUBROUTINE ATRDE(B,N,M,D,L)
	DIMENSION B(M),D(N)
	DOUBLE PRECISION B,D
	L=1
	IF (M.NE.(3*N-2)) THEN
	  L=-1
	  WRITE(*,10)
	  RETURN
	END IF
10	FORMAT(1X,'  ERR  ')
	DO 20 K=1,N-1
	  J=3*K-2
	  IF (ABS(B(J))+1.0.EQ.1.0) THEN
	    L=0
	    WRITE(*,10)
	    RETURN
	  END IF
	  B(J+1)=B(J+1)/B(J)
	  D(K)=D(K)/B(J)
	  B(J+3)=B(J+3)-B(J+2)*B(J+1)
	  D(K+1)=D(K+1)-B(J+2)*D(K)
20	CONTINUE
	IF (ABS(B(3*N-2))+1.0.EQ.1.0) THEN
	  L=0
	  WRITE(*,10)
	  RETURN
	END IF
	D(N)=D(N)/B(3*N-2)
	DO 30 K=N-1,1,-1
	  D(K)=D(K)-B(3*K-1)*D(K+1)
30	CONTINUE
	RETURN
	END







!
subroutine table()
implicit none
include 'rcommon.h'
		write(*,7001)pois_init,pois_onf,pois_fix
		7001 Format(4x,"Poisson Fix:",5x,I2,5x,I2,"/",I4)
	if(pnum>0)then
		write(*,7000)pnum
		7000 Format(4x,"Pulse Number:",5x,I2)
	end if
	if(tgt1==1)then
		write(*,8000)snume1,celle1,snumi1,celli1
		8000 Format(4x,"Tgt-1,Number:",5x,"E:",I8,"/",I4,5x,"I:",I8,"/",I4)
		write(*,8001)tgtx1*1e6,tgty1*1e6,honum1,grad1
		8001 Format(11x,"x1*y1:",F5.1,"*",F5.1,5x,"Hole:",I1,5x,"Grad:",I1)
	end if
	if(tgt2==1)then
		write(*,9000)snume2,celle2,snumi2,celli2
		9000 Format(4x,"Tgt-2,Number:",5x,"E:",I8,"/",I4,5x,"I:",I8,"/",I4)
		write(*,9001)tgtx2*1e6,tgty2*1e6,honum2,grad2
		9001 Format(11x,"x2*y2:",F5.1,"*",F5.1,5x,"Hole:",I1,5x,"Grad:",I1)
	end if
	write(*,10020)totstep,totime*1e15,totimet
	10020 Format(4x,"Steps:",I7,4x,"Times:",F6.1," fs / ",F6.1," T")
	write(*,10000)dt/per1,ctx,cty,dx*1e9,dy*1e9
	10000 Format(4x,"dt:",F7.5," T",3x,"Ctx:",F4.2,2x,"Cty:",F4.2,3x,"dx*dy:",F4.0,"*",F4.0," nm")
	write(*,10010)lx*1e6,ly*1e6,xnum,ynum
	10010 Format(4x,"X*Y:",F6.1,"*",F6.1," um",3x,"Grid:",I5,"*",I5)
return
end subroutine


!速度初始化：
!根据粒子的初始温度来初始化粒子的速率，vx,vy,vz
!计算粒子的初始动量，相对论因子，能量
subroutine init_v(cnum,vxy,pxy,gama,cm,rm,init,eng)
	implicit none
	include 'rcommon.h'
	integer cnum,it,init
	real*8 rd1,rd2
	real*8 pxy(4,cnum),vxy(4,cnum),gama(cnum),eng(cnum),cm,rm,vv
	!将粒子的初始化温度转化成速度，值得推敲
	vv=sqrt(dble(init)*1e3*trans/rm)
	!Maxwel速率分布方法，有待验证其正确性
	call random_seed()
	do it=1,cnum
		call random_number(rd1)
		call random_number(rd2)
		vxy(2,it)=2d0*vv*sqrt(rd1*(1d0-rd1))*cos(2d0*pi*rd2)
		vxy(3,it)=2d0*vv*sqrt(rd1*(1d0-rd1))*sin(2d0*pi*rd2)
		vxy(4,it)=2d0*vv*(1d0-2d0*rd2)
		!设置固定的粒子速度，可以用于检验程序，非检验的时候一定要屏蔽了
		!	vxy(2,it)=2e7
		!	vxy(3,it)=2e8
		!	vxy(4,it)=2e6
	end do
	!下面计算中，数组里面出现的冒号，值的是数组里对应的所有量
	vxy(1,:)=sqrt(vxy(2,:)**2+vxy(3,:)**2+vxy(4,:)**2)	!粒子速度
	gama(:)=1./sqrt(1.-(vxy(1,:)/co)**2)	!粒子相对论因子
	eng(:)=rm/me*(gama(:)-1d0)*0.511d0	!粒子能量，单位MeV
	!检验伽马的初始化，如果有任何一个粒子的gama为0或者为无穷（NaN）就停止
	if(any(gama==0.).or.any(isnan(gama)))then
		write(*,*)"gama 初始化错误(子程序:init_v)"
		stop
	end if
	!计算模拟粒子的动量
	pxy(2,:)=vxy(2,:)*cm*gama(:)
	pxy(3,:)=vxy(3,:)*cm*gama(:)
	pxy(4,:)=vxy(4,:)*cm*gama(:)
	pxy(1,:)=sqrt(pxy(2,:)**2+pxy(3,:)**2+pxy(4,:)**2)
return
end subroutine

!初始化位置：
!根据HOLE子程序里得到的粒子所在格子信息数组（ij）来往格子里随机分配固定个数的粒子，同时
!记录粒子的分格信息，Z方向的位置初始化完全等同于Y方向
!同时，根据粒子位置的X值，用gradient梯度函数计算出粒子的权重，得到模拟粒子的电量和质量
subroutine init_xy(cell,cnum,wt,m,qo,ij,xy,cm,cq,pij,dist,weit,tgtmx,tgtx)
	implicit none
	include 'rcommon.h'
	integer cnum,i,j,rt,im,ip,iq,cell
	integer ij(xnum+1,ynum+1),pij(3,cnum)
	real*8,external::gradient	!申明gradient子函数
	real*8 xy(3,cnum),cm(cnum),cq(cnum),weit(cnum),dist(2,cnum),x,y,wt,m,qo
	real*8 tgtmx,tgtx,gt
	real*8 rd1,rd2,rdc1,rdc2
	ip=0
	!对整个计算区域的格点循环
	do i=1,xnum+1
		do j=1,ynum+1
			!如果这个格子里有粒子就处理
			if(ij(i,j)==1)then
				!在这个格子里，循环cell次，就是放置的模拟粒子的个数
				do im=1,cell
					call random_number(rd1)	!获取一个0到1之间的随机数
					x=(dble(i-1)+rd1)*dx	!用这个随机数计算粒子在这个格子
											!的什么位置,进而计算出这个粒子的
											!横坐标
					!这样用随机数求出的粒子的横坐标，在边界上的时候由于数值计算
					!上的误差，有可能得到的横坐标超出靶设置的范围，所有要检验x
					!是否在规定范围内，若不在则修正，否则相对论因子会为0，在求
					!权重时出现错误
					if(dble(x)<dble(tgtmx))then!如果出现在靶左边界的左边了
						do rt=1,100	!循环100次，进行修正
							call random_number(rdc1)
							x=x+rdc1*dx*0.8d0	!用现在的坐标+一个格子宽度*0.8*随机数，
											!这样可以保证粒子坐标落在现在需要
											!分配的格子内
							if(dble(x)>dble(tgtmx))then
								exit	!一旦满足要求了就跳出循环
							end if
						end do
					end if
					if(dble(x)>dble(tgtmx+tgtx))then!如果出现在靶右边界的右边了
						do rt=1,100	!循环100次，进行修正
							call random_number(rdc2)
							x=x-rdc2*dx*0.8d0	!用现在的坐标+一个格子宽度*0.8*随机数，
											!这样可以保证粒子坐标落在现在需要
											!分配的格子内
							if(dble(x)<dble(tgtmx+tgtx))then
								exit	!一旦满足要求了就跳出循环
							end if
						end do
					end if
					iq=im+ip*cell	!计算现在的这个粒子在所有粒子中的序号
					xy(1,iq)=x	!写入横坐标
					gt=gradient(x)!把横坐标代入gradient函数，计算这个粒子的权重因子
					if(gt==0d0)then!检查权重是否出现0，若为0了，就提示错误并结束程序
						write(*,*)"离子密度梯度错误(子程序:init_xy)",x,gt
						stop
					end if
					weit(iq)=gt	!写入粒子权重
					cm(iq)=wt*m*gt	!用这个权重计算这个模拟粒子的质量
					cq(iq)=wt*qo*gt	!计算这个模拟粒子的电量
					!随机分配这个格子中粒子的纵坐标
					call random_number(rd2)
				!	rd2=0.4
					y=(rd2+dble(j-1))*dy
					xy(2,iq)=y
					xy(3,iq)=y	!粒子的z坐标?????????
					!计算这个粒子所在格子的i和j
					pij(1,iq)=floor(dble(x)/dx)+1
					pij(2,iq)=floor(dble(y)/dy)+1
					!计算离子坐标与格子边界距离，是与粒子所在格子的左边界和下边界的距离
					!这个距离不是实际距离，而是与单个格子宽度或高
					!度的比值，乘上dx或dy后就为实际距离，用real函数让计算中的数值类型统一,
					!不用也可以，计算中会自动执行，但这是一种习惯，可以避免有可能发生的一些
					!不容易检查的错误
					dist(1,iq)=x/dx-dble(pij(1,iq)-1)
					dist(2,iq)=y/dy-dble(pij(2,iq)-1)
					!判断计算的距离是否出错，有上面的计算可以知道，wx、wy的范围应该在0、1之间
					!作这个判断也是为了检验在粒子分格上是否出现了数值计算上的误差
					if(dist(1,iq)<0d0.or.dist(1,iq)>1d0.or.dist(2,iq)<0d0.or.dist(2,iq)>1d0)then
						write(*,*)"与格子边界距离初始化错误",dist(1,iq),dist(2,iq)
						stop
					end if
				end do
				ip=ip+1	!加1，一个格子循环完毕了
			end if
		end do
	end do
return
end subroutine

!靶形状（挖洞）：
!根据靶的初始参量中的洞参数来确定粒子所在的格子，返回一个储存粒子位置信息的数组（ij）
subroutine hole(ij,tgtmx,tgtmy,tgtx,tgty,honum,tgt)
	implicit none
	include 'rcommon.h'
	integer it,chk,i,j,im,ig,ip,i1,j1
	integer ist,jst,iend,jend,honum,tgt
	integer ij(xnum+1,ynum+1)
	real*8 tx,ty,tempx,tempy,ix,jy,tgtmx,tgtmy,tgtx,tgty
	!计算一个完整的靶所占的格子
	!计算靶的左边界所在的格子，ceiling为大于等于的整数
	ist=floor(tgtmx/dx)+1
	!计算靶右边界所在的格子，floor为小于等于的整数
	iend=floor((tgtmx+tgtx)/dx)
	!计算靶的下边界所在的格子
	jst=floor((tgtmy-tgty*0.5d0)/dy)+1
	!计算靶的上边界所在的格子
	jend=floor((tgtmy+tgty*0.5d0)/dy)
	!write(*,*)ist,iend,jst,jend
	!检验靶的放置是否超出计算区域
	if(ist<0.or.iend>xnum+1.or.jst<0.or.jend>ynum+1)then
		write(*,*)"靶",tgt," 大小设置错误"
		stop
	end if
	ij=0	!把整个标识数组归0
	ij(ist:iend,jst:jend)=1	!把完整靶所占的格子归1
	if(honum==0)then	!如果这个靶没有设置洞就返回
		return
	end if
	!对整个计算区域的格子循环
	do i=1,xnum+1
		do j=1,ynum+1
			if(ij(i,j)==1)then
			!如果这个格子的标识为1，即有粒子，就计算出这个格子的正中心的坐标
				ix=(dble(i)-0.5d0)*dx
				jy=(dble(j)-0.5d0)*dy
			!如果现在处理的是靶1，开始挖洞，其实所谓的挖洞就是如果现在ij数组中为1
			!也就是有粒子的格子，如果正好处在了被挖的区域则赋值为0就行了，所以下面
			!的挖洞要按顺序处理，因为挖后面的孔是在前面挖完孔后的ij数组基础上再处理的
			!注意，这里传递的ij不是数组，而是外面主循环决定的那一个格子ij(i,j)上的值
				if(tgt==1)then
					if(honum>=1)then!挖第一个洞，把关于靶1第一个洞得参数都传递过去，
									!返回的是被处理过的ij
    call hole_shape(hoshp1,ix,jy,hox1,hoy1,hosiz1,howid1,hogs1,tgtx,ij(i,j),honf1,&
                    hocp1x1,hocp1y1,hodg11,hocp1x2,hocp1y2,hodg12,horf11,horf12,horf13)
					end if
					if(honum>=2)then!如果洞得个数多于2个，就再挖第二个洞，在被
									!第一个洞处理过的ij数组上，再处理，再次返回ij
    call hole_shape(hoshp2,ix,jy,hox2,hoy2,hosiz2,howid2,hogs2,tgtx,ij(i,j),honf2,&
                    hocp2x1,hocp2y1,hodg21,hocp2x2,hocp2y2,hodg22,horf21,horf22,horf23)
					end if
					if(honum>=3)then!如果洞得个数多于3个，就再挖第三个洞，在被
									!第二个洞处理过的ij数组上，再处理，再次返回ij
    call hole_shape(hoshp3,ix,jy,hox3,hoy3,hosiz3,howid3,hogs3,tgtx,ij(i,j),honf3,&
                    hocp3x1,hocp3y1,hodg31,hocp3x2,hocp3y2,hodg32,horf31,horf32,horf33)
					end if
				end if

			!如果现在处理的是靶2，整个过程和靶1的处理时一样的
 				if(tgt==2)then
					if(honum>=1)then
    call hole_shape(hoshp4,ix,jy,hox4,hoy4,hosiz4,howid4,hogs4,tgtx,ij(i,j),honf4,&
                    hocp4x1,hocp4y1,hodg41,hocp4x2,hocp4y2,hodg42,horf41,horf42,horf43)
					end if
					if(honum>=2)then
    call hole_shape(hoshp5,ix,jy,hox5,hoy5,hosiz5,howid5,hogs5,tgtx,ij(i,j),honf5,&
                    hocp5x1,hocp5y1,hodg51,hocp5x2,hocp5y2,hodg52,horf51,horf52,horf53)
					end if
					if(honum>=3)then
    call hole_shape(hoshp6,ix,jy,hox6,hoy6,hosiz6,howid6,hogs6,tgtx,ij(i,j),honf6,&
                    hocp6x1,hocp6y1,hodg61,hocp6x2,hocp6y2,hodg62,horf61,horf62,horf63)
					end if
				end if
 			end if
		end do
	end do
return
end subroutine

!洞的形状：
!由靶的初始参量来判断洞的形状，同时把洞所在地方的格子标示信息归0（挖洞）
!洞有三种：圆（任意位置），前表面高斯，后表面高斯。（其中高斯可以通过hogs参量任意改变高
!斯形状,如超高斯（4），亚高斯（1）等等，当hogs很大时近似于矩形），通过这样的设计，
!就想尽量设计出比较独特的靶的形状
subroutine hole_shape(hoshp,ix,jy,hox,hoy,hosiz,howid,hogs,tgtx,chk,honf,&
                      hocpx1,hocpy1,hodg1,hocpx2,hocpy2,hodg2,horf1,horf2,horf3)
	implicit none
	include 'rcommon.h'
	integer hoshp,hogs,chk,honf
	real*8 ix,jy,hox,hoy,hosiz,howid,tgtx
	integer horf1,horf2,horf3
	real*8 hocpx1,hocpy1,hodg1,hocpx2,hocpy2,hodg2
	real*8 tempx1,tempx2,tempx3,tempy2,tempy3
	real*8 k1,k2,k3,temp1,temp2,temp3
	logical bs1,bs2,bs3	!逻辑变量，true或者false
	real*8,external::gauss	!调用了自定义的function，在这里需要声明
	!传递过来的ij(i,j)值如果已经为0了，即已经被挖了，就不处理了， 返回
	if(chk==0)then
		return
	end if
	!在这里选择你挖的孔的形状进行处理，包括圆形，高斯形状，线性规划，具体的处理我就不
	!详细写了，其实我现在自己看都感觉有些晕乎，可能是当时头脑发热做出的一些不考虑后果
	!的事情,主要是一些逻辑上的问题；处理得思路就是判断要处理得这个格子的中心是不是在
	!被挖的这个孔的形状之内，如果在就返回ij(i,j)=0，如果不在就返回ij(i,j)=1，
	!这些判断主要是数学函数上的问题了。
	select case(hoshp)
	!圆形
		case(0)
			tempx1=(ix-hox)**2+(jy-hoy)**2
			if(tempx1<=hosiz**2)then
				if(honf==0)then!范围内的，舍掉
					chk=0
				else if(honf==1)then!范围内的，保留
					chk=1
				end if
			else
				if(honf==1)then!范围外的，舍掉
					chk=0
				else if(honf==0)then!范围外的，保留
					chk=1
				end if
			end if
	!左开口高斯
		case(1)
			tempy2=jy-hoy
			tempx2=hosiz*gauss(tempy2,howid,hogs)
			if(ix<=tempx2+hox)then
				if(honf==0)then!范围内的，舍掉
					chk=0
				else if(honf==1)then!范围内的，保留
					chk=1
				end if
			else
				if(honf==1)then!范围外的，舍掉
					chk=0
				else if(honf==0)then!范围外的，保留
					chk=1
				end if
			end if
	!右开口高斯
		case(2)
			tempy3=jy-hoy
			tempx3=hosiz*gauss(tempy3,howid,hogs)
			if(ix>=hox-tempx3)then
				if(honf==0)then!范围内的，舍掉
					chk=0
				else if(honf==1)then!范围内的，保留
					chk=1
				end if
			else
				if(honf==1)then!范围外的，舍掉
					chk=0
				else if(honf==0)then!范围外的，保留
					chk=1
				end if
			end if
	!线性规划
		case(3)
			if(k1==90.or.k2==90)then!判断直线斜率是否为90,若是则报错
				write(*,*)"线性规划错误，直线斜率不能为90度"
				stop
			end if
			k1=tan(hodg1/180d0*pi)!第一条直线的斜率
			temp1=k1*(ix-hocpx1)+hocpy1!求出给定横坐标在第一条直线上的纵坐标
			if((jy<=temp1.and.horf1==0).or.(jy>=temp1.and.horf1==1))then
				bs1=.true.!如果纵坐标在直线下方，同时horf为0或者纵坐标在直线上方，同时horf为1，为真
			else
				bs1=.false.!否则为假
			end if
			k2=tan(hodg2/180d0*pi)!第二条直线的斜率
			temp2=k2*(ix-hocpx2)+hocpy2!求出给定横坐标在第二条直线上的纵坐标
			if((jy<=temp2.and.horf2==0).or.(jy>=temp2.and.horf2==1))then
				bs2=.true.!如果纵坐标在直线下方，同时horf为0或者纵坐标在直线上方，同时horf为1，为真
			else
				bs2=.false.!否则为假
			end if
			if(hocpx1==hocpx2)then!对第三条直线处理，如为垂直直线，则用判断左右的方式确定
				if((ix<=hocpx1.and.horf3==0).or.(ix>=hocpx1.and.horf3==1))then
					bs3=.true.
				else
					bs3=.false.
				end if
			else
				k3=(hocpy2-hocpy1)/(hocpx2-hocpx1)!若有斜率，则用求斜率求方程的方法
				temp3=k3*(ix-hocpx1)+hocpy1!求出给定横坐标在第三条直线上的纵坐标
				if((jy<=temp3.and.horf3==0).or.(jy>=temp3.and.horf3==1))then
					bs3=.true.!如果纵坐标在直线下方，同时horf为0或者纵坐标在直线上方，同时horf为1，为真
				else
					bs3=.false.!否则为假
				end if
			end if
			if(bs1.and.bs2.and.bs3)then!如果同时满足三条直线的约束，都为真
				if(honf==0)then!规划的部分，舍掉
					chk=0
				else if(honf==1)then!规划的部分，保留
					chk=1
				end if
			else
				if(honf==1)then!规划外的部分，舍掉
					chk=0
				else if(honf==0)then!规划外的部分，保留
					chk=1
				end if
			end if
	end select
return
end subroutine



!
subroutine laser()
	implicit none
	include 'rcommon.h'
	integer i,j,inflag
	real*8 tempfix
	real*8 pley1(ynum),pley2(ynum)
	real*8 prey1(ynum),prey2(ynum)
	real*8 plez1(ynum+1),plez2(ynum+1)
	real*8 prez1(ynum+1),prez2(ynum+1)

	if(pnum>0)then
		pley1=0d0
		pley2=0d0
		plez1=0d0
		plez2=0d0
		prey1=0d0
		prey2=0d0
		prez1=0d0
		prez2=0d0
		inflag=0
	if(step==0)then
		if(udshape1<3)then
			p1t1=tup1*per1      	!上升时间
			p1t2=p1t1+tdur1*per1    !上升时间+持续时间
			p1t3=p1t2+tdown1*per1	!上升时间+持续时间+下降时间
			pfin1=pstart1*per1+p1t3      !脉冲结束时间
		else
			p1t1=ptop1*per1
			p1t2=ptao1*per1
			pfin1=pstart1*per1+10d0*p1t2
		end if

		if(udshape2<3)then
			p2t1=tup2*per2      	!上升时间
			p2t2=p2t1+tdur2*per2    !上升时间+持续时间
			p2t3=p2t2+tdown1*per2	!上升时间+持续时间+下降时间
			pfin2=pstart2*per2+p2t3      !脉冲结束时间
		else
			p2t1=ptop2*per2
			p2t2=ptao2*per2
			pfin2=pstart2*per2+10d0*p2t2
		end if

	end if

		!时间处在第1个脉冲出现的时间范围内
		if(pnum>=1.and.rtime>=pstart1*per1.and.rtime<=pfin1)then
			inflag=1
			!处理第1个脉冲
			call pulse(pley1,prey1,plez1,prez1,&
							gs1,polar1,posit1,side1,udshape1,a1,phase1,r1,lamda1,womiga1,pstart1,p1t1,p1t2,p1t3)
		endif

		!时间处在第2个脉冲出现的时间范围内
		if(pnum>=2.and.rtime>=pstart2*per2.and.rtime<=pfin2)then
			inflag=1
			!处理第2个脉冲
			call pulse(pley2,prey2,plez2,prez2,&
							gs2,polar2,posit2,side2,udshape2,a2,phase2,r2,lamda2,womiga2,pstart2,p2t1,p2t2,p2t3)
		endif

		if(inflag==1)then
			!把所有脉冲的电磁场都相加，分左右两边
			tempfix=2.0*ctx



		i=0.5*xnum
		j=0.5*ynum

	!	fdez(i,:)= fdez(i,:)+ sin(rtime*womiga)
!		fdez(i,:)= sin((step+1)*dt*womiga)*co

!	fdbz(:,j)= sin((step+1)*dt*womiga)!*co
	!	fdey(i,:)= sin((step+1)*dt*womiga)*co

	!laserl=0.5*xnum
			fdey(laserl,:)=fdey(laserl,:)+(pley1+pley2)*tempfix
			fdez(laserl,:)=fdez(laserl,:)+(plez1+plez2)*tempfix
			fdey(laserr,:)=fdey(laserr,:)+(prey1+prey2)*tempfix
			fdez(laserr,:)=fdez(laserr,:)+(prez1+prez2)*tempfix
			!输出激光源的
			open(114,file='laser.txt',position='append')
				write(114,*)rtime*1d15,fdey(laserl,ynum/2+1),fdez(laserl,ynum/2+1),&
							sqrt(fdey(laserl,ynum/2+1)**2+fdez(laserl,ynum/2+1)**2)
			close(114,status='keep')
		end if
	endif


return
end subroutine

!
subroutine pulse(pley,prey,plez,prez,gs,polar,positm,side,udshape,amp,phase,radio,lamda,wmga,pstart,pt1,pt2,pt3)
	implicit none
	include 'rcommon.h'
	integer gs,polar,side,udshape
	real*8 pley(ynum),prey(ynum),plez(ynum+1),prez(ynum+1)
	real*8 amp,positm,posit,phase,radio,lamda,wmga,pstart,pt1,pt2,pt3
	integer i
	real*8 multi,ofey,ofez,y,phh,rraa
	real*8 dftime,amplit,io
	real*8 ppey(ynum),ppez(ynum+1),pey(ynum),pez(ynum+1)
	real*8 pex(ynum+1),pby(ynum+1)
	real*8 emax,eymax,ezmax
	posit=positm*ly
	pley=0d0
	prey=0d0
	plez=0d0
	prez=0d0
	!处理偏振方向
	select case(polar)
		case(0)	!Cycle
			polarsign=0
			multi=2d0
			ofey=1d0
			ofez=1d0
		case(1) !P
			polarsign=1
			multi=1d0
			ofey=1d0
			ofez=0d0
		case(2) !S
			polarsign=1
			multi=1d0
			ofey=0d0
			ofez=1d0
	end select
	!把振幅转换为功率密度
	dftime=rtime-pstart
	if(udshape==0)then
		if(dftime<=pt1)then
			!如果时间处在上升阶段，振幅为随着时间变化的正弦函数
			amplit=amp*sin(0.5d0*pi*dftime/pt1)**0.5
		else if(dftime>pt1.and.dftime<pt2)then
			!如果时间处在持续阶段，振幅持续为最大
			amplit=amp
		else if(dftime>=pt2)then
			!如果时间处在下降阶段，振幅为随着时间变化的正弦函数，此时为下降，所以应该时间
			!越变大，振幅越变小
			amplit=amp*sin(0.5d0*pi*(pt3-dftime)/(pt3-pt2))**0.5
		end if
	else if(udshape==1)then
		if(dftime<=pt1)then
			!如果时间处在上升阶段，振幅为随着时间变化的正弦函数
			amplit=amp*sin(0.5d0*pi*dftime/pt1)
		else if(dftime>pt1.and.dftime<pt2)then
			!如果时间处在持续阶段，振幅持续为最大
			amplit=amp
		else if(dftime>=pt2)then
			!如果时间处在下降阶段，振幅为随着时间变化的正弦函数，此时为下降，所以应该时间
			!越变大，振幅越变小
			amplit=amp*sin(0.5d0*pi*(pt3-dftime)/(pt3-pt2))
		end if
	else if(udshape==2)then
		if(dftime<=pt1)then
			amplit=amp*dftime/pt1
		else if(dftime>pt1.and.dftime<pt2)then
			amplit=amp
		else if(dftime>=pt2.and.dftime<=pt3)then
			amplit=amp*(pt3-dftime)/(pt3-pt2)
		end if



	else if(udshape==3)then
		amplit=amp*exp(-1d0*((dftime-pt1)/pt2)**2)
	end if
	!!!!!!!!!!!
	io=1.37d10*(amplit/lamda)**2
	!功率密度转换为电场
	emax=sqrt(2.0d0*io/co/ebsn)
	phh=womiga*dftime+phase*pi
	eymax=sin(phh)*emax
	ezmax=cos(phh)*emax
	!输出激光源的
	open(115,file='io.txt',position='append')
		write(115,*)rtime*co*1d6,io,eymax,ezmax,emax
	close(115,status='keep')
	!电场Ey
	do i=1,ynum
		!计算循环到的这个格点与中心的距离
		y=abs(posit-(i-0.5d0)*dy)
		ppey(i)=eymax*sqrt(exp(-1d0*(y/radio)**gs))
	end do
	!电场Ez
	do i=1,ynum+1
		y=abs(posit-(i-1d0)*dy)
		ppez(i)=ezmax*sqrt(exp(-1d0*(y/radio)**gs))
	end do
	!!!!!!!!!!!!!!!!!!!!!!!!!!
	!区分偏振
	pey=ppey*ofey
	pez=ppez*ofez
	!返回左右两边的电磁场
	select case(side)
		case(0)
			pley=pey
			prey=0d0
			plez=pez
			prez=0d0
		case(1)
			pley=0d0
			prey=pey
			plez=0d0
			prez=pez
	end select

	if(teonf==0)then
		pley=0d0
		prey=0d0
	end if

	if(tmonf==0)then
		plez=0d0
		prez=0d0
	end if

return
end subroutine



!把半格点上的场平均到整格点上
subroutine average()
	implicit none
	include 'rcommon.h'
	if(teonf==1)then
		!EX
		!左右列等于左右
		fdex1(1,:)=fdex(1,:)
		fdex1(xnum+1,:)=fdex(xnum,:)
		!中间点取左右平均
		fdex1(2:xnum,:)=(fdex(1:xnum-1,:)+fdex(2:xnum,:))*0.5d0
		!EY
		!上下列等于上下
		fdey1(:,1)=fdey(:,1)
		fdey1(:,ynum+1)=fdey(:,ynum)
		!中间点取上下平均
		fdey1(:,2:ynum)=(fdey(:,1:ynum-1)+fdey(:,2:ynum))*0.5d0
		!BZ
		!四个角点等于角点
		fdbz1(1,1)=fdbz(1,1)
		fdbz1(1,ynum+1)=fdbz(1,ynum)
		fdbz1(xnum+1,1)=fdbz(xnum,1)
		fdbz1(xnum+1,ynum+1)=fdbz(xnum,ynum)
		!左右边界等于左右边界上下取平均
		fdbz1(1,2:ynum)=(fdbz(1,1:ynum-1)+fdbz(1,2:ynum))*0.5d0
		fdbz1(xnum+1,2:ynum)=(fdbz(xnum,1:ynum-1)+fdbz(xnum,2:ynum))*0.5d0
		!上下边界等于上下边界左右取平均
		fdbz1(2:xnum,1)=(fdbz(1:xnum-1,1)+fdbz(2:xnum,1))*0.5d0
		fdbz1(2:xnum,ynum+1)=(fdbz(1:xnum-1,ynum)+fdbz(2:xnum,ynum))*0.5d0
		!中间点等于上下左右四点取平均
		fdbz1(2:xnum,2:ynum)=(fdbz(1:xnum-1,1:ynum-1)+fdbz(2:xnum,1:ynum-1)+ &
							fdbz(1:xnum-1,2:ynum)+fdbz(2:xnum,2:ynum))*0.25d0
	end if
	if(tmonf==1)then
		!BX
		fdbx1(:,1)=fdbx(:,2)
		fdbx1(:,ynum+1)=fdbx(:,ynum)
		fdbx1(:,2:ynum)=(fdbx(:,1:ynum-1)+fdbx(:,2:ynum))*0.5d0
		!BY
		fdby1(1,:)=fdby(1,:)
		fdby1(xnum+1,:)=fdby(xnum,:)
		fdby1(2:xnum,:)=(fdby(1:xnum-1,:)+fdby(2:xnum,:))*0.5d0
		!EZ不用平均，本来就在整格点上
	end if
	call cpu_time(cput2)
return
end subroutine


subroutine energy()
	implicit none
	include 'rcommon.h'
	real*8 engf,engfe,engfm
	real*8 engp,engp1,engp2,engi1t,engi2t,enge1t,enge2t
	integer i,j
	!
!	call average()


	engp=0d0
	if(tgt1==1)then
		engi1t=0d0
		do i=1,cnumi1
			engi1t=engi1t+engi1(i)*wti1*weiti1(i)
		end do
		!单位从MeV转换成J
		engi1t=engi1t*1.602d-13
		enge1t=0d0
		do i=1,cnume1
			enge1t=enge1t+enge1(i)*wte1*weite1(i)
		end do
		enge1t=enge1t*1.602d-13
		engp1=engi1t+enge1t
	end if
	if(tgt2==1)then
		engi2t=0d0
		do i=1,cnumi2
			engi2t=engi2t+engi2(i)*wti2*weiti2(i)
		end do
		engi2t=engi2t*1.602d-13
		enge2t=0d0
		do i=1,cnume2
			enge2t=enge2t+enge2(i)*wte2*weite2(i)
		end do
		enge2t=enge2t*1.602d-13
		engp2=engi2t+enge2t
	end if

	engp=engp1+engp2
	!计算TE场的能量
	engfe=0d0
	if(teonf==1)then
		do i=laserl+10,xnum
			do j=2,ynum
				engfe=engfe+0.5d0*(ebsn*(fdex(i,j)**2+fdey(i,j)**2)+fdbz(i,j)**2/miu)*s
		!		engfe=engfe+0.5d0*(fdbz(i,j)**2/miu)*s
			end do
		end do




	end if
	!计算TM场的能量
	engfm=0d0
	if(tmonf==1)then
		do i=laserl+10,xnum
			do j=2,ynum
				engfm=engfm+0.5d0*((fdbx(i,j)**2/miu+fdby(i,j)**2)+ebsn*fdez(i,j)**2)*s
			end do
		end do
	end if
	engf=engfe+engfm

	open(113,file='energy.txt',position='append')
		write(113,*)rtime*1d15,engfe,engfm,engf,engp,engp+engf
	close(113)


return
end subroutine










!粒子模块：
!计算粒子的运动，及电流密度
subroutine particle()
	implicit none
	include 'rcommon.h'
	integer i,j
	!判断是否计算靶1
	if(tgt1==1)then
	!靶1离子部分
		call cpu_time(cputp1)
		call part_move(cnumi1,piji1b,piji1,disti1,cmi1,cqi1,gamai1,pxyi1,vxyi1,xyi1b,xyi1,iconi1,mi1,angi1,engi1)
		call cpu_time(cputp2)
		cputp12=cputp2-cputp1
		call current(cqi1,cnumi1,piji1b,piji1,xyi1b,xyi1,vxyi1,cutxi1,cutyi1,cutzi1,iconi1)
		call cpu_time(cputp3)
		cputp23=cputp3-cputp2
		call part_move(cnume1,pije1b,pije1,diste1,cme1,cqe1,gamae1,pxye1,vxye1,xye1b,xye1,icone1,me,ange1,enge1)
		call cpu_time(cputp4)
		cputp34=cputp4-cputp3
		call current(cqe1,cnume1,pije1b,pije1,xye1b,xye1,vxye1,cutxe1,cutye1,cutze1,icone1)
		call cpu_time(cputp5)
		cputp45=cputp5-cputp4
	end if
	!靶2的离子和电子
	if(tgt2==1)then
		call cpu_time(cputp6)
		call part_move(cnumi2,piji2b,piji2,disti2,cmi2,cqi2,gamai2,pxyi2,vxyi2,xyi2b,xyi2,iconi2,mi2,angi2,engi2)
		call cpu_time(cputp7)
		cputp67=cputp7-cputp6
		call current(cqi2,cnumi2,piji2b,piji2,xyi2b,xyi2,vxyi2,cutxi2,cutyi2,cutzi2,iconi2)
		call cpu_time(cputp8)
		cputp78=cputp8-cputp7
		call part_move(cnume2,pije2b,pije2,diste2,cme2,cqe2,gamae2,pxye2,vxye2,xye2b,xye2,icone2,me,ange2,enge2)
		call cpu_time(cputp9)
		cputp89=cputp9-cputp8
		call current(cqe2,cnume2,pije2b,pije2,xye2b,xye2,vxye2,cutxe2,cutye2,cutze2,icone2)
		call cpu_time(cputp10)
		cputp90=cputp10-cputp9
	end if
	!sum the current
	cutx=cutxi1+cutxi2+cutxe1+cutxe2
	cuty=cutyi1+cutyi2+cutye1+cutye2
	cutz=cutzi1+cutzi2+cutze1+cutze2
	!如果电磁场上下边界为周期性边界，则对电流密度施加周期性边界
	if(udbd==0)then
		cutx(:,1)=cutx(:,1)+cutx(:,ynum+1)
		cutx(:,ynum+1)=cutx(:,1)
		cutz(:,1)=cutz(:,1)+cutz(:,ynum+1)
		cutz(:,ynum+1)=cutz(:,1)
	end if

	!write(*,*)sum(cutx),sum(cuty),sum(cutz)


return
end subroutine

!粒子运动：
!用面积权重法将平均到整格点上的场分配到粒子身上
!粒子边界条件处理后重新分格
subroutine part_move(cnum,pijb,pij,dist,cm,cq,gama,pxy,vxy,xyb,xy,icon,rm,ang,eng)
	implicit none
	include 'rcommon.h'
	integer cnum,it,i,j
	integer pijb(3,cnum),pij(3,cnum),icon(cnum)
	real*8 wt(4),cm(cnum),cq(cnum),rm,gama(cnum),pxy(4,cnum),dist(2,cnum)
	real*8 vxy(4,cnum),xyb(3,cnum),xy(3,cnum),ang(cnum),eng(cnum)
	real*8 ex,ey,bz,bx,by,ez
	real*8 weight(4),temp(4)
	real*8,external::array
	!初始归零，这几个量代表的是分配到单个粒子上的电磁场大小
	ex=0d0
	ey=0d0
	bz=0d0
	bx=0d0
	by=0d0
	ez=0d0
	!备份位置信息，在计算之前进行备份，也就是上次运动计算出的结果，直接用数组
	!想等的方式赋值，因为这两对数组在维度大小上是完全一致的
	!for the other current weighting method
	pijb=pij
	xyb=xy
	!开始处理粒子运动
	!对粒子逐个循环计算
	do it=1,cnum
		!若这个粒子已经标识为没出界，则计算
		if(icon(it)==1)then
			!取出粒子的分格坐标
			i=pij(1,it)
			j=pij(2,it)
			!计算权重
			call weighting(dist(:,it),weight)
			!write(*,*)weight(1:4)
			!利用权重求出粒子所在处的场
			!如果计算TE模
			if(teonf==1)then
				temp(1)=fdex1(i,j)
				temp(2)=fdex1(i+1,j)
				temp(3)=fdex1(i+1,j+1)
				temp(4)=fdex1(i,j+1)
				ex=array(weight,temp)

				!write(*,*)temp(1:4),ex

				temp(1)=fdey1(i,j)
				temp(2)=fdey1(i+1,j)
				temp(3)=fdey1(i+1,j+1)
				temp(4)=fdey1(i,j+1)
				ey=array(weight,temp)

				temp(1)=fdbz1(i,j)
				temp(2)=fdbz1(i+1,j)
				temp(3)=fdbz1(i+1,j+1)
				temp(4)=fdbz1(i,j+1)
				bz=array(weight,temp)
			end if
			!如果计算TM模
			if(tmonf==1)then
				temp(1)=fdbx1(i,j)
				temp(2)=fdbx1(i+1,j)
				temp(3)=fdbx1(i+1,j+1)
				temp(4)=fdbx1(i,j+1)
				bx=array(weight,temp)

				temp(1)=fdby1(i,j)
				temp(2)=fdby1(i+1,j)
				temp(3)=fdby1(i+1,j+1)
				temp(4)=fdby1(i,j+1)
				by=array(weight,temp)

				temp(1)=fdez(i,j)
				temp(2)=fdez(i+1,j)
				temp(3)=fdez(i+1,j+1)
				temp(4)=fdez(i,j+1)
				ez=array(weight,temp)
			end if
			!注意：上面的TE模和TM模是可以同时都计算的

			!求解粒子运动方程
			!采用旋转法（三步）
			call part_go(gama(it),cq(it),cm(it),rm,ex,ey,bz,bx,by,ez,&
						pxy(1:4,it),vxy(1:4,it),xy(1:3,it),ang(it),eng(it))
			!调用子程序，传递的参量：
			!注意：调用的这个子程序是求解单个粒子的运动，所以没有出界的每个，粒子都会
			!	调用计算，所以传递过去的参量都是单个模拟粒子的信息，而不是数组
			!	gama(it):单个模拟粒子的相对论因子,
			!	cq(it)：单个模拟粒子的电荷,
			!	cm(it)：单个模拟粒子的质量,
			!	rm：实际质量,不用数组，因为一种粒子的实际质量肯定都是一样的（模拟粒
			!		子的可以不一样，因为有可能受密度梯度所带来的权重的影响）
			!	ex,ey,bz,bx,by,ez：电磁场大小,
			!	pxy(1:4,it)：单个模拟粒子的动量，P、Px、Py、Pz,
			!	vxy(1:4,it)：单个模拟粒子的速度，V、Vx、Vy、Vz,
			!	xy(1:3,it)：单个模拟粒子的坐标，X、Y、Z,
			!	ang(it)：单个模拟粒子的偏转角,
			!	eng(it)：单个模拟粒子其中一个真实粒子的能量
		end if
		!处理粒子边界条件
		call part_bound(xy(1:3,it),vxy(1:4,it),icon(it))
		!更新了粒子的运动后（包括粒子边界条件处理后），重新计算粒子所在格子
		if(icon(it)==1)then
		!如果粒子出界了则不进行下面的计算
			i=floor(xy(1,it)/dx)+1
			j=floor(xy(2,it)/dy)+1
			!把计算出来的分格信息写入到数组
			pij(1,it)=i
			pij(2,it)=j
			!用更新后的粒子坐标和所在格子，计算新的粒子与所在格子的左边界和下边界的距离
			dist(1,it)=xy(1,it)/dx-dble(i-1)
			dist(2,it)=xy(2,it)/dy-dble(j-1)
		end if
	end do
return
end subroutine




!求解粒子的运动，更新粒子的动量，速度，位置，相对论因子
!采用半加速，旋转，半加速的方式
!参考的文章题目为：2.5维等离子体模拟分布式并行程序设计（粒子MAXWEL速度分布的部分也是这篇文章）
!但是，文章中的公式，有一个地方有错误，大概的地方就是文章中公式5下面的那句话中的两个公式，
!有一个应该应该为+号，(我不确定是不是第二个了，从下面的代码中可以验证是哪个)，
!这种解法是可以简单推导的，而且LPIC++的程序说明上也有部分的说明，你可以结合看一下
subroutine part_go(gm,qa,m,rm,ex,ey,bz,bx,by,ez,pxy,vxy,xy,ang,eng)
			!相对论因子，电荷，质量，真实质量，电磁场，动量，速度，位置，散射角，能量
	implicit none
	include 'rcommon.h'
	integer judge,js(3)
	real*8 gm,qa,m,rm,ex,ey,bz,bx,by,ez,eng
	real*8 pxy(4),vxy(4),xy(3),ang,deg
	real*8 px,py,pz,vx,vy,vz,x,y,z
	real*8 sgm,sgmx,sgmy,sgmz,sgmx2,sgmy2,sgmz2,sgmxy,sgmyz,sgmzx,sgmxyz
	real*8 ux,uy,uz,unx,uny,unz,unx2,uny2,unz2,gmds,rmgm,qdt2m,qtmex,qtmey,qtmez
	!一个常用式子
	qdt2m=0.5*qa*dt/m
	!计算 u-
	qtmex=qdt2m*ex
	qtmey=qdt2m*ey
	qtmez=qdt2m*ez
	ux=vxy(2)*gm+qtmex
	uy=vxy(3)*gm+qtmey
	uz=vxy(4)*gm+qtmez
	!计算gama，相对论因子
	gm=sqrt(1d0+(ux**2+uy**2+uz**2)/(co**2))
	!计算 sigma
	sgm=qdt2m/gm
	sgmx=sgm*bx
	sgmy=sgm*by
	sgmz=sgm*bz
	sgmx2=sgmx**2
	sgmy2=sgmy**2
	sgmz2=sgmz**2
	sgmxy=sgmx*sgmy
	sgmyz=sgmy*sgmz
	sgmzx=sgmz*sgmx
	sgmxyz=1d0/(1d0+sgmx2+sgmy2+sgmz2)
	!
	!计算 u+
	unx=((1d0+sgmx2-sgmy2-sgmz2)*ux+       2d0*(sgmz+sgmxy)*uy+       2d0*(sgmzx-sgmy)*uz)*sgmxyz
	uny=(       2d0*(sgmxy-sgmz)*ux+(1.0-sgmx2+sgmy2-sgmz2)*uy+       2d0*(sgmx+sgmyz)*uz)*sgmxyz
	unz=(       2d0*(sgmy+sgmzx)*ux+       2d0*(sgmyz-sgmx)*uy+(1d0-sgmx2-sgmy2+sgmz2)*uz)*sgmxyz
	!
	!计算u+1/2
	unx2=unx+qtmex
	uny2=uny+qtmey
	unz2=unz+qtmez
	!更新gama
	gm=sqrt(1d0+(unx2**2+uny2**2+unz2**2)/co**2)
	!更新速度
	vx=unx2/gm
	vy=uny2/gm
	vz=unz2/gm
	!保存速度
	vxy(1)=sqrt(vx**2+vy**2+vz**2)
	vxy(2)=vx
	vxy(3)=vy
	vxy(4)=vz
	!更新粒子坐标
	xy(1)=xy(1)+vx*dt
	xy(2)=xy(2)+vy*dt
	xy(3)=xy(3)+vz*dt
	!计算真实粒子动量
	rmgm=rm*gm
	pxy(2)=rmgm*vx
	pxy(3)=rmgm*vy
	pxy(4)=rmgm*vz
	pxy(1)=sqrt(pxy(2)**2+pxy(3)**2+pxy(4)**2)
	!计算真实粒子能量
	!eng=rm*(gm-1.0)*co**2/trans
	eng=(gm-1d0)*dble(rm/me)*0.511d0
	!偏转角度
	deg=vy/(vx+1d-10)
		!-90 ~ +90,right
	if(vx>=0)then
		ang=atan(deg)*180d0/pi
	endif
	if(vx<0)then
		if(vy>=0)then
			!+90 ~ +180
			ang=atan(deg)*180d0/pi+180d0
		else
			!-90 ~ -180
			ang=atan(deg)*180d0/pi-180d0
		end if
	end if
	!if(sqrt(vx**2+vy**2+vz**2)>co)then
	!	write(*,*)"离子推动错误:",vx,vy,vz,sqrt(vx**2+vy**2+vz**2),gm
	!	stop
	!end if
return
end subroutine



!粒子边界条件：
!判断粒子是否出界
!采用周期性边界条件（上出下进）
subroutine part_bound(xy,vxy,icon)
	implicit none
	include 'rcommon.h'
	integer icon
	real*8 xy(3),vxy(4),x,y,z
	!取出粒子的坐标
	x=xy(1)
	y=xy(2)
	z=xy(3)
	!Y方向
	!周期边界
	if(pboundud==0)then
		if(y<0d0)then
			xy(2)=ly+y
		end if
		if(y>=ly)then
			xy(2)=y-ly
		end if

	else if(pboundud==1)then
		if(y<0d0.or.y>ly)then
			icon=0
			return
		end if
	end if
	!X方向
	!周期边界
	if(pboundlr==0)then
		if(x<0d0)then
			xy(1)=lx+x
		end if
		if(x>lx)then
			xy(1)=x-lx
		end if
	else if(pboundlr==1)then
		if(x<0d0.or.x>lx)then
			icon=0
			return
		end if
	end if
return
end subroutine







!采用分段比例的方式分配粒子电流密度
!粒子电流密度到格点电流密度：
!根据粒子的速度和运动前后的格子关系把粒子的电流密度分配到半格点上
subroutine current(qa,cnum,pijb,pij,xyb,xy,vxy,cutxo,cutyo,cutzo,icon)
	implicit none
	include 'rcommon.h'
	integer i1,i2,j1,j2,i,j
	integer cnum,pijb(3,cnum),pij(3,cnum),it,icon(cnum)
	real*8 wtlong,wtlong1,wtlong2,wtlong3,wtlong1t,wtlong2t,kl
	real*8 jfx,jfy,jfz
	real*8 cutxo(xnum,ynum+1),cutyo(xnum+1,ynum),cutzo(xnum+1,ynum+1)
	real*8 xyb(3,cnum),xy(3,cnum),vxy(4,cnum),qa(cnum)
	real*8 x1,x2,y1,y2,vx,vy,nq,xo,yo,xoo,yoo,xp,yp
	integer j1p,j2p
	real*8 y1p,y2p
	cutxo=0d0
	cutyo=0d0
	cutzo=0d0
	do it=1,cnum
		!若出界标识为真（没出界）
		if(icon(it)==1)then
			!取出粒子运动前后的分格信息
			i1=pijb(1,it)
			j1=pijb(2,it)
			i2=pij(1,it)
			j2=pij(2,it)
			!取出粒子运动前后的位置信息
			!把坐标全部归一化到dx,dy,把值扩大
			x1=dble(xyb(1,it))/dx
			y1=dble(xyb(2,it))/dy
			x2=dble(xy(1,it))/dx
			y2=dble(xy(2,it))/dy
			!计算单个粒子的电流密度
			nq=qa(it)/s
			jfx=nq*vxy(2,it)
			jfy=nq*vxy(3,it)
			jfz=nq*vxy(4,it)
			!开始分配jx,jy
			!横向无穿越(分两种情况：1.纵向也无穿越,还在原来的格子里）
							!	2.纵向有穿越,上下运动)
			!新的格子横坐标=旧的格子横坐标，也就是横向无穿越
			if(i2==i1)then
				!情况1：同一个格子，新的格子纵坐标=旧的格子纵坐标
				if(j2==j1)then
					!长度权重为 1，由于没有穿越格子，所以只用处理一段线段，故权重为1
					wtlong=1d0 	!这里赋值为1d0,因为这个量定义的是双精度型，次段
								!子程序中有一些量都定义成了双精度型（real*8）,
								!都是为了尽量保证计算中的精度
					call current_xy(i1,j1,wtlong,x1,y1,x2,y2,jfx,jfy,jfz,cutxo,cutyo,cutzo,1)
				!情况2：不在同一个格子
				!使用总坐标差的绝对值为1来判断是不是一定在原来格子的上面或者下面格子
				else if(abs(j2-j1)==1)then
					!下面的计算是为了求出粒子运动轨迹与格子边界的交点坐标，与横线交点
					!运动轨迹的斜率倒数，j1/=j2保证了y2/=y1，即分母不为0
					kl=(x2-x1)/(y2-y1)
					!交点横/纵坐标
					yo=dble(min(j1,j2))
					xo=x1+kl*(yo-y1)
					!对于原来的格子中的那段线段
					!长度比例
					wtlong=(y1-yo)/(y1-y2)
					call current_xy(i1,j1,wtlong,x1,y1,xo,yo,jfx,jfy,jfz,cutxo,cutyo,cutzo,2)
					!对于新到的格子中的那段线段
					!长度比例
					wtlong=1d0-wtlong
					call current_xy(i1,j2,wtlong,xo,yo,x2,y2,jfx,jfy,jfz,cutxo,cutyo,cutzo,3)
				end if
			end if
			!横向有穿越（再分两种情况：3.纵向无穿越，左右运动
							      !4.纵向有穿越，穿越三个格子，最麻烦的情况）
			if(abs(i2-i1)==1)then
				!粒子运动轨迹的斜率,由于i2/=i1，保证了分母不为0
				kl=(y2-y1)/(x2-x1)
				!情况3：<左右关系>
				if(j2==j1)then
					!求出运动轨迹与格子边界交点的坐标，与竖线交点
					xo=dble(min(i1,i2))
					yo=y1+kl*(xo-x1)
					!对于在原来格子的那段线段
					!长度比列
					wtlong=(x1-xo)/(x1-x2)
					call current_xy(i1,j1,wtlong,x1,y1,xo,yo,jfx,jfy,jfz,cutxo,cutyo,cutzo,4)
					!对于新到格子的那段线段
					wtlong=1d0-wtlong
					call current_xy(i2,j1,wtlong,xo,yo,x2,y2,jfx,jfy,jfz,cutxo,cutyo,cutzo,5)
				!情况4：穿越三个格子
				else if(abs(j2-j1)==1)then
					!运动轨迹与格子边界交点的坐标，与竖线交点
					xo=dble(min(i1,i2))
					yo=y1+kl*(xo-x1)

					!运动轨迹与格子边界交点的坐标，与横线交点
					yoo=dble(min(j1,j2))
					xoo=x1+(yoo-y1)/kl
					!
					!运动轨迹起始点(x1,y1)与竖线交点(xo,yo)之间距离占总长度的比例
					wtlong1t=(x1-xo)/(x1-x2)
					!运动轨迹起始点(x1,y1)与横线交点(xoo,yoo)之间距离占总长度的比例
					wtlong2t=(x1-xoo)/(x1-x2)
					!write(*,*)i1,i2,xo,yo,j1,j2,xoo,yoo,kl,wtlong1t,wtlong2t
					!用比较上面的两个比例的大小的方法来确定交点的顺序，是先穿过横线
					!还是先穿过竖线，需要确定这个是因为在调用current_xy子程序时，
					!需要明确的一小段线段所在的格子坐标，线段的起始点坐标和终点坐标
					!如果是先经过（xo,yo）点（竖线），再经过（xoo,yoo）点(横线)
					if(wtlong1t<=wtlong2t)then
						!由于顺序确定了，就可以确定三段的长度比列
						wtlong1=wtlong1t
						wtlong2=wtlong2t-wtlong1t
						wtlong3=1d0-wtlong2t
						!write(*,*)"z"
	 				else
					!如果是先经过（xoo,yoo）点（横线），再经过（xo,yo）点
						!确定三段的长度比列
						!write(*,*)"h"
						wtlong1=wtlong2t
						wtlong2=wtlong1t-wtlong2t
						wtlong3=1d0-wtlong1t
						!调换点的坐标，保证第一个交点的坐标存放在（xo,yo）中，
						!第二个交点的坐标存放在（xoo,yoo）中
						xp=xo
						yp=yo
						xo=xoo
						yo=yoo
						xoo=xp
						yoo=yp
					end if
					!用两个交点的中点坐标来求经过那个格子的坐标
					i=ceiling(0.5d0*(xo+xoo))
					j=ceiling(0.5d0*(yo+yoo))
					!write(*,*)i,j
					!分别计算三小段（三个格子坐标，三个起始点和终点，三个权重）
					call current_xy(i1,j1,wtlong1,x1 ,y1 ,xo ,yo ,jfx,jfy,jfz,cutxo,cutyo,cutzo,6)
					call current_xy(i ,j ,wtlong2,xo ,yo ,xoo,yoo,jfx,jfy,jfz,cutxo,cutyo,cutzo,7)
					call current_xy(i2,j2,wtlong3,xoo,yoo,x2 ,y2 ,jfx,jfy,jfz,cutxo,cutyo,cutzo,8)
				end if
			end if
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			!处理上下周期边界的情况
			if(pboundud==0)then
				if(abs(j2-j1)==ynum-1)then
					!横向所在格子不变，只用分配到上下两个格子
					if(i2==i1)then
						!从下面到上面
						if(j2>j1)then
							y1p=y1+dble(ynum)
							j1p=j1+ynum
							kl=(x2-x1)/(y2-y1p)
							!交点横/纵坐标
							yo=dble(j2)
							xo=x1+kl*(yo-y1p)
							!对于原来的格子中的那段线段
							!长度比例
							wtlong=(y1p-yo)/(y1p-y2)
							call current_xy(i1,j1,wtlong,x1,y1,xo,yo-dble(ynum),jfx,jfy,jfz,cutxo,cutyo,cutzo,9)
							!对于新到的格子中的那段线段
							!长度比例
							wtlong=1d0-wtlong
							call current_xy(i2,j2,wtlong,xo,yo,x2,y2,jfx,jfy,jfz,cutxo,cutyo,cutzo,10)
						else if(j2<j1)then
						!从上面到下面
							y2p=y2+dble(ynum)
							j2p=j2+ynum
							kl=(x2-x1)/(y2p-y1)
							!交点横/纵坐标
							yo=dble(j1)
							xo=x1+kl*(yo-y1)
							!对于原来的格子中的那段线段
							!长度比例
							wtlong=(y1-yo)/(y1-y2p)
							call current_xy(i1,j1,wtlong,x1,y1,xo,yo,jfx,jfy,jfz,cutxo,cutyo,cutzo,11)
							!对于新到的格子中的那段线段
							!长度比例
							wtlong=1d0-wtlong
							call current_xy(i2,j2,wtlong,xo,yo-dble(ynum),x2,y2,jfx,jfy,jfz,cutxo,cutyo,cutzo,12)
						end if
					!横向所在格子也变了，要分配到三个格子
					else if(abs(i2-i1)==1)then
						!从下面到上面
						if(j2>j1)then
							y1p=y1+dble(ynum)
							j1p=j1+ynum
							kl=(y2-y1p)/(x2-x1)
							!运动轨迹与格子边界交点的坐标，与竖线交点
							xo=dble(min(i1,i2))
							yo=y1p+kl*(xo-x1)
							!运动轨迹与格子边界交点的坐标，与横线交点
							yoo=dble(j2)
							xoo=x1+(yoo-y1p)/kl
							!
							!运动轨迹起始点(x1,y1)与竖线交点(xo,yo)之间距离占总长度的比例
							wtlong1t=(x1-xo)/(x1-x2)
							!运动轨迹起始点(x1,y1)与横线交点(xoo,yoo)之间距离占总长度的比例
							wtlong2t=(x1-xoo)/(x1-x2)

							!用两个交点的中点坐标来求经过那个格子的坐标
							i=ceiling(0.5d0*(xo+xoo))
							j=ceiling(0.5d0*(yo+yoo))
							!write(*,*)i1,i2,xo,yo,j1,j2,xoo,yoo,kl,wtlong1t,wtlong2t
							!用比较上面的两个比例的大小的方法来确定交点的顺序，是先穿过横线
							!还是先穿过竖线，需要确定这个是因为在调用current_xy子程序时，
							!需要明确的一小段线段所在的格子坐标，线段的起始点坐标和终点坐标
							!如果是先经过（xo,yo）点（竖线），再经过（xoo,yoo）点(横线)
							if(wtlong1t<=wtlong2t)then
								!由于顺序确定了，就可以确定三段的长度比列
								wtlong1=wtlong1t
								wtlong2=wtlong2t-wtlong1t
								wtlong3=1d0-wtlong2t
								!分别计算三小段（三个格子坐标，三个起始点和终点，三个权重）
								call current_xy(i1,j1,wtlong1,x1 ,y1 ,xo ,yo-dble(ynum) ,jfx,jfy,jfz,cutxo,cutyo,cutzo,13)
								call current_xy(i ,j-ynum ,wtlong2,xo ,yo-dble(ynum) ,xoo,yoo-dble(ynum),jfx,jfy,jfz,cutxo,cutyo,cutzo,14)
								call current_xy(i2,j2,wtlong3,xoo,yoo,x2 ,y2 ,jfx,jfy,jfz,cutxo,cutyo,cutzo,15)
							else
							!如果是先经过（xoo,yoo）点（横线），再经过（xo,yo）点
								!确定三段的长度比列
								wtlong1=wtlong2t
								wtlong2=wtlong1t-wtlong2t
								wtlong3=1d0-wtlong1t
							!分别计算三小段（三个格子坐标，三个起始点和终点，三个权重）
							call current_xy(i1,j1,wtlong1,x1 ,y1 ,xoo ,yoo-dble(ynum) ,jfx,jfy,jfz,cutxo,cutyo,cutzo,16)
							call current_xy(i ,j ,wtlong2,xoo ,yoo ,xo,yo,jfx,jfy,jfz,cutxo,cutyo,cutzo,17)
							call current_xy(i2,j2,wtlong3,xo,yo,x2 ,y2 ,jfx,jfy,jfz,cutxo,cutyo,cutzo,18)
							end if
						!从上面到下面
						else if(j2<j1)then
							y2p=y2+dble(ynum)
							j2p=j2+ynum
							kl=(y2p-y1)/(x2-x1)
							!运动轨迹与格子边界交点的坐标，与竖线交点
							xo=dble(min(i1,i2))
							yo=y1+kl*(xo-x1)
							!运动轨迹与格子边界交点的坐标，与横线交点
							yoo=dble(j1)
							xoo=x1+(yoo-y1)/kl
							!
							!运动轨迹起始点(x1,y1)与竖线交点(xo,yo)之间距离占总长度的比例
							wtlong1t=(x1-xo)/(x1-x2)
							!运动轨迹起始点(x1,y1)与横线交点(xoo,yoo)之间距离占总长度的比例
							wtlong2t=(x1-xoo)/(x1-x2)

							!用两个交点的中点坐标来求经过那个格子的坐标
							i=ceiling(0.5d0*(xo+xoo))
							j=ceiling(0.5d0*(yo+yoo))
							!write(*,*)i1,i2,xo,yo,j1,j2,xoo,yoo,kl,wtlong1t,wtlong2t
							!用比较上面的两个比例的大小的方法来确定交点的顺序，是先穿过横线
							!还是先穿过竖线，需要确定这个是因为在调用current_xy子程序时，
							!需要明确的一小段线段所在的格子坐标，线段的起始点坐标和终点坐标
							!如果是先经过（xo,yo）点（竖线），再经过（xoo,yoo）点(横线)
							if(wtlong1t<=wtlong2t)then
								!由于顺序确定了，就可以确定三段的长度比列
								wtlong1=wtlong1t
								wtlong2=wtlong2t-wtlong1t
								wtlong3=1d0-wtlong2t
								!分别计算三小段（三个格子坐标，三个起始点和终点，三个权重）
								call current_xy(i1,j1,wtlong1,x1 ,y1 ,xo ,yo,jfx,jfy,jfz,cutxo,cutyo,cutzo,19)
								call current_xy(i ,j ,wtlong2,xo ,yo ,xoo,yoo,jfx,jfy,jfz,cutxo,cutyo,cutzo,20)
								call current_xy(i2,j2,wtlong3,xoo,yoo-dble(ynum),x2 ,y2 ,jfx,jfy,jfz,cutxo,cutyo,cutzo,21)
							else
							!如果是先经过（xoo,yoo）点（横线），再经过（xo,yo）点
								!确定三段的长度比列
								wtlong1=wtlong2t
								wtlong2=wtlong1t-wtlong2t
								wtlong3=1d0-wtlong1t
								!分别计算三小段（三个格子坐标，三个起始点和终点，三个权重）
								call current_xy(i1,j1,wtlong1,x1 ,y1 ,xoo ,yoo,jfx,jfy,jfz,cutxo,cutyo,cutzo,22)
								call current_xy(i ,j-ynum ,wtlong2,xoo ,yoo-dble(ynum) ,xo,yo-dble(ynum),jfx,jfy,jfz,cutxo,cutyo,cutzo,23)
								call current_xy(i2,j2,wtlong3,xo,yo-dble(ynum),x2 ,y2 ,jfx,jfy,jfz,cutxo,cutyo,cutzo,24)
							end if
						end if
					end if
				end if
			end if
			!!!!!!!!!!!!!!!!!!!!!!!
		end if
	end do
return
end subroutine

!处理单个格子里的线段对这个格子的格点的电流密度的贡献：
subroutine current_xy(i,j,wtlong,x1,y1,x2,y2,jfx,jfy,jfz,cutxo,cutyo,cutzo,nn)
	implicit none
	include 'rcommon.h'
	integer i,j,nn
	real*8 jfx,jfy,jfz,wjfx,wjfy,wjfz
	real*8 wtlong,x1,y1,x2,y2
	real*8 xr1,yr1,xr2,yr2,tppx,tppy,temp1,temp2
	real*8 midx,midy,mid1,mid2,mid3,mid4
	real*8 cutxo(xnum,ynum+1),cutyo(xnum+1,ynum),cutzo(xnum+1,ynum+1)

	tppx=dble(i-1)
	tppy=dble(j-1)
	!求出起始点和终点与边界的距离,dx
	xr1=x1-tppx
	yr1=y1-tppy
	xr2=x2-tppx
	yr2=y2-tppy

	!
!	if(wtlong<0d0.or.wtlong>1d0)then
!		write(*,*)"1",wtlong,nn
!	end if
!	!检验粒子与边界的距离是否正确
!	if(xr1<0d0.or.xr1>1d0)then
!		write(*,*)"2",x1,y1,x2,y2,i-1,j-1,nn
!	end if
!	if(xr2<0d0.or.xr2>1d0)then
!		write(*,*)"3",x1,y1,x2,y2,i-1,j-1,nn
!	end if
!	if(yr1<0d0.or.yr1>1d0)then
!		write(*,*)"4",x1,y1,x2,y2,i-1,j-1,nn
!	end if
!	if(yr2<0d0.or.yr2>1d0)then
!		write(*,*)"5",x1,y1,x2,y2,i-1,j-1,nn
!	end if

	!电流密度jx
	temp1=0.5d0*(yr1+yr2)
	wjfx=wtlong*jfx
	cutxo(i,j)=cutxo(i,j)+wjfx*(1d0-temp1)
	cutxo(i,j+1)=cutxo(i,j+1)+wjfx*temp1
	!电流密度jy
	temp2=0.5d0*(xr1+xr2)
	wjfy=wtlong*jfy
	cutyo(i,j)=cutyo(i,j)+wjfy*(1d0-temp2)
	cutyo(i+1,j)=cutyo(i+1,j)+wjfy*temp2
	!电流密度jz
	midx=0.5d0*(xr1+xr2)
	midy=0.5d0*(yr1+yr2)
	mid1=midx*midy
	mid2=(1d0-midx)*midy
	mid4=midx*(1d0-midy)
	mid3=1d0-mid1-mid2-mid4
	wjfz=wtlong*jfz
	cutzo(i,j)=cutzo(i,j)+wjfz*mid3
	cutzo(i+1,j)=cutzo(i+1,j)+wjfz*mid4
	cutzo(i+1,j+1)=cutzo(i+1,j+1)+wjfz*mid1
	cutzo(i,j+1)=cutzo(i,j+1)+wjfz*mid2
return
end subroutine









!解Maxwel和场的边界条件：
!分别求解TE和TM的Maxwel方程组
!TE中，BZ推动三次，EXY推动两次，BZ边界条件处理三次
!TM中，BXY推动三次，EZ推动两次，EZ边界条件处理两次（二阶MUR）
subroutine maxwel()
	implicit none
	include 'rcommon.h'
	integer i,j
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if(teonf==1)then
		!(0--1/4)
		call tebd_backup()!备份边界上的BZ，为求边界条件用
		call bz(0.25d0,0.25d0)!Bz(0--1/4)
		call tebd(fparx,fpary,jd3)!边界条件
		call exy()!Ex(0--1/2) Ey(0--1/2)
		!(1/4--3/4)
		call tebd_backup()!备份边界上的BZ，为求边界条件用
		call bz(0.5d0,0.5d0)!Bz(1/4--3/4)
		call tebd(hparx,hpary,jd4)!边界条件
		call exy()!Ex(1/2--1) Ey(1/2--1)
		!(3/4--1)
		call tebd_backup()!备份边界上的BZ，为求边界条件用
		call bz(0.25d0,0.25d0)!Bz(3/4--1)
		call tebd(fparx,fpary,jd3)!边界条件
	end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TM!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if(tmonf==1)then
		!0-1/4
		call bxy(0.25d0,0.25d0)!Bx/By 0-1/4
		call tmbd_backup()!备份边界上的EZ
		call ez()!Ez 0-1/2
		call tmbd()
		!1/4-3/4
		call bxy(0.5d0,0.5d0)!Bx/By 1/4-3/4
		call tmbd_backup()!备份边界上的EZ
		call ez()!Ez 1/2-1
		call tmbd()
		!3/4-1
		call bxy(0.25d0,0.25d0)!Bx/By 3/4-1
	end if
return
end subroutine



subroutine tebd_backup()
	implicit none
	include 'rcommon.h'
	tempbzl(:)=fdbz(2,:)
	tempbzr(:)=fdbz(xnum-1,:)
	tempbzd(:)=fdbz(:,2)
	tempbzu(:)=fdbz(:,ynum-1)
return
end subroutine

subroutine bz(tempx,tempy)
	implicit none
	include 'rcommon.h'
	integer i,j
	real*8 tempx,tempy
	do j=2,ynum-1
		do i=2,xnum-1
			fdbz(i,j)=fdbz(i,j)+tempy*tpy*(fdex(i,j+1)-fdex(i,j))-tempx*tpx*(fdey(i+1,j)-fdey(i,j))
		end do
	end do
	!Period
	if(udbd==0)then
		do i=1,xnum
			fdbz(i,1)=fdbz(i,1)+tempy*tpy*(fdex(i,2)-fdex(i,1))-tempx*tpx*(fdey(i+1,1)-fdey(i,1))
			fdbz(i,ynum)=fdbz(i,1)
		end do
	end if
return
end subroutine

subroutine exy()
	implicit none
	include 'rcommon.h'
	integer i,j
	!Ex
	do j=2,ynum
		do i=1,xnum
			fdex(i,j)=fdex(i,j)+0.5d0*(tpey*(fdbz(i,j)-fdbz(i,j-1))-det*cutx(i,j))
		end do
	end do
	!Period
	if(udbd==0)then
		do i=1,xnum
			fdex(i,1)=fdex(i,1)+0.5d0*(tpey*(fdbz(i,1)-fdbz(i,ynum))-det*cutx(i,1))
			fdex(i,ynum+1)=fdex(i,1)
		end do
	end if
	!Ey
	do j=1,ynum
		do i=2,xnum
			fdey(i,j)=fdey(i,j)-0.5d0*(tpex*(fdbz(i,j)-fdbz(i-1,j))+det*cuty(i,j))
		end do
	end do
return
end subroutine

subroutine tebd(parx,pary,jd)
	implicit none
	include 'rcommon.h'
	real*8 parx,pary,jd
	integer i,j
	!>>>>>>>>>>>>>>>>>>>>> left and right <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	!MUR-1
	if(lrbd==1)then
		do j=2,ynum-1
			fdbz(1,j)=tempbzl(j)+parx*(fdbz(2,j)-fdbz(1,j))
			fdbz(xnum,j)=tempbzr(j)+parx*(fdbz(xnum-1,j)-fdbz(xnum,j))
		end do
	end if

	!>>>>>>>>>>>>>>>>>>>>> up and down <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	!MUR-1
	if(udbd==1)then
		do i=2,xnum-1
			fdbz(i,1)=tempbzd(i)+pary*(fdbz(i,2)-fdbz(i,1))
			fdbz(i,ynum)=tempbzu(i)+pary*(fdbz(i,ynum-1)-fdbz(i,ynum))
		end do
	!左下角
	fdbz(1,1)=tempbzl(2)+jd3*(fdbz(2,2)-fdbz(1,1))
	!左上角
	fdbz(1,ynum)=tempbzl(ynum-1)+jd3*(fdbz(2,ynum-1)-fdbz(1,ynum))
	!右下角
	fdbz(xnum,1)=tempbzr(2)+jd3*(fdbz(xnum-1,2)-fdbz(xnum,1))
	!右上角
	fdbz(xnum,ynum)=tempbzr(ynum-1)+jd3*(fdbz(xnum-1,ynum-1)-fdbz(xnum,ynum))
	end if
	if(udbd==0)then
	!左下角
	fdbz(1,1)=fdbz(1,2)
	!左上角
	fdbz(1,ynum)=fdbz(1,ynum-1)
	!右下角
	fdbz(xnum,1)=fdbz(xnum,2)
	!右上角
	fdbz(xnum,ynum)=fdbz(xnum,ynum-1)
	end if
return
end subroutine

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine bxy(tempx,tempy)
	implicit none
	include 'rcommon.h'
	integer i,j
	real*8 tempx,tempy

	do j=1,ynum
		do i=2,xnum
			fdbx(i,j)=fdbx(i,j)-tempy*tpy*(fdez(i,j+1)-fdez(i,j))
		end do
	end do

	do j=2,ynum
		do i=1,xnum
			fdby(i,j)=fdby(i,j)+tempx*tpx*(fdez(i+1,j)-fdez(i,j))
		end do
	end do
	!Period
	if(udbd==0)then
		do i=1,xnum
			fdby(i,1)=fdby(i,1)+tempx*tpx*(fdez(i+1,1)-fdez(i,1))
			fdby(i,ynum+1)=fdby(i,1)
		end do
	end if
return
end subroutine

subroutine ez()
	implicit none
	include 'rcommon.h'
	integer i,j
	do j=2,ynum
		do i=2,xnum
			fdez(i,j)=fdez(i,j)+0.5d0*(tpex*(fdby(i,j)-fdby(i-1,j))&
				-tpey*(fdbx(i,j)-fdbx(i,j-1))-det*cutz(i,j))
		end do
	end do
return
end subroutine

subroutine tmbd_backup()
	implicit none
	include 'rcommon.h'
  	tempezl(:)=fdez(2,:)
	tempezr(:)=fdez(xnum,:)
	tempezd(:)=fdez(:,2)
	tempezu(:)=fdez(:,ynum)
return
end subroutine

subroutine tmbd()
	implicit none
	include 'rcommon.h'
	integer i,j
	!left and right
	!Mur-1
	if(lrbd==1)then
		do j=2,ynum
			fdez(1,j)=tempezl(j)+hparx*(fdez(2,j)-fdez(1,j))
			fdez(xnum+1,j)=tempezr(j)+hparx*(fdez(xnum,j)-fdez(xnum+1,j))
		end do
	end if

	!up and down
	!Period
	if(udbd==0)then
		do i=2,xnum
			fdez(i,1)=fdez(i,1)+0.5d0*(tpex*(fdby(i,1)-fdby(i-1,1))&
				-tpey*(fdbx(i,1)-fdbx(i,ynum))-det*cutz(i,1))
			fdez(i,ynum+1)=fdez(i,1)
		end do
	end if
	!MUR-1
	if(udbd==1)then
		do i=2,xnum
			fdez(i,1)=tempezd(i)+hpary*(fdez(i,2)-fdez(i,1))
			fdez(i,ynum+1)=tempezu(i)+hpary*(fdez(i,ynum)-fdez(i,ynum+1))
		end do
	end if
	!MUR-2
	if(udbd==2)then

	end if
	!corner
	if(udbd>0)then
		fdez(1,1)=tempezl(2)+jd4*(fdez(2,2)-fdez(1,1))
		fdez(xnum+1,1)=tempezd(xnum)+jd4*(fdez(xnum,2)-fdez(xnum+1,1))
		fdez(xnum+1,ynum+1)=tempezr(ynum)+jd4*(fdez(xnum,ynum)-fdez(xnum+1,ynum+1))
		fdez(1,ynum+1)=tempezu(2)+jd4*(fdez(2,ynum)-fdez(1,ynum+1))
	else
		fdez(1,1)=fdez(1,2)
		fdez(1,ynum+1)=fdez(1,ynum)
		fdez(xnum+1,ynum+1)=fdez(xnum+1,ynum)
		fdez(xnum+1,1)=fdez(xnum+1,2)
	end if

return
end subroutine


!solve the poisson and fix the E
subroutine fix_poisson()
	implicit none
	include 'rcommon.h'
	integer j,k,m,j1
	integer FFTL,errl
	real*8 fftinr(ynum),fftini(ynum)
	real*8 fftoutr(ynum),fftouti(ynum)
	integer,parameter::xnum1=xnum+1,ynum1=ynum+1,trde=3*xnum1-2
	real*8 rowr(xnum1,ynum),rowi(xnum1,ynum),rowtr(xnum1),rowti(xnum1)
	real*8 linea(trde),lineain(trde),fair(xnum1,ynum1),faii(xnum1,ynum1)
	real*8 dm,rvalue,rmax,rmin,tempp,tempt
	rowr=0d0
	rowi=0d0
	rowtr=0d0
	rowti=0d0
	!求格点上的电荷密度
	call sum_row()
	do j=2,xnum
		do k=2,ynum
			row(j,k)=row(j,k)-((fdex(j,k)-fdex(j-1,k))/dx+(fdey(j,k)-fdey(j,k-1))/dy)*ebsn
		end do
	end do
	if(udbd==0)then
		row(:,1)=0d0
		row(:,ynum+1)=row(:,1)
	end if

	!电荷密度在Y方向进行FFT变换
	do j=1,xnum1
		fftinr(1:ynum)=row(j,1:ynum)*dy
		fftini=0d0
		FFTL=0
		call FFT(fftinr,fftini,ynum,rowr(j,1:ynum),rowi(j,1:ynum),FFTL)
	end do
	!P(j,m)
	tempp=dx**2/ebsn
	rowr=rowr*tempp
	rowi=rowi*tempp
	!当左右为开放边界时
	if(pois_type==1)then
		!当dm=1(m=1,最下面一行)时，有解析解
		do j=1,xnum1
			rowtr(j)=0d0
			rowti(j)=0d0
			do j1=1,xnum1
				rowtr(j)=rowtr(j)-0.5d0*(dble(abs(j1-j))*rowr(j1,1))
				rowti(j)=rowti(j)-0.5d0*(dble(abs(j1-j))*rowi(j1,1))
			end do
		end do
		rowr(1:xnum1,1)=rowtr(1:xnum1)
		rowi(1:xnum1,1)=rowti(1:xnum1)
		!其他行
		do m=2,ynum
			dm=1d0+2d0*(dx/dy*sin(pi*(m-1)/ynum))**2
			rvalue=sqrt(dm*dm-1d0)
			rmax=dm+rvalue
			rmin=dm-rvalue
			linea=0d0
			linea(1)=rmax
			linea(2)=-1d0
			linea(trde-1)=-1d0
			linea(trde)=rmax
			do j=3,trde-4,3
				linea(j)=-1d0
				linea(j+1)=2d0*dm
				linea(j+2)=-1d0
			end do
			lineain=linea
			call ATRDE(lineain,xnum1,trde,rowr(1:xnum1,m),errl)
			call ATRDE(linea  ,xnum1,trde,rowi(1:xnum1,m),errl)
		end do
	end if
	!当左右边界电势为0时
	if(pois_type==0)then
		do m=1,ynum
			dm=1d0+2d0*(dx/dy*sin(pi*(m-1)/ynum))**2
			linea=0d0
			linea(1)=2d0
			linea(2)=-1d0
			linea(trde-1)=-1d0
			linea(trde)=2d0
			do j=3,trde-4,3
				linea(j)=-1d0
				linea(j+1)=2d0*dm
				linea(j+2)=-1d0
			end do
			lineain=linea
			call ATRDE(lineain,xnum1,trde,rowr(1:xnum1,m),errl)
			call ATRDE(linea  ,xnum1,trde,rowi(1:xnum1,m),errl)
		end do
	end if
	!对电势进行逆FFT变换
	do j=1,xnum1
		fftinr(1:ynum)=rowr(j,1:ynum)
		fftini(1:ynum)=rowi(j,1:ynum)
		fftoutr=0d0
		fftouti=0d0
		FFTL=1
		call FFT(fftinr,fftini,ynum,fftoutr,fftouti,FFTL)
		fair(j,1:ynum)=fftoutr(1:ynum)/dy
		fair(j,ynum1)=fair(j,1)
		faii(j,1:ynum)=fftouti(1:ynum)/dy
		faii(j,ynum1)=faii(j,1)
	end do
	!得到半格点上的电场
	do j=1,xnum
		do k=1,ynum1
			fdex(j,k)=fdex(j,k)+(fair(j,k)-fair(j+1,k))/dx
		end do
	end do
	do j=1,xnum1
		do k=1,ynum
			fdey(j,k)=fdey(j,k)+(fair(j,k)-fair(j,k+1))/dy
		end do
	end do
	!output
	!输出修正的电势分布
!	open(11,file='fai.txt')
!	do j=1,xnum1
!		do k=1,ynum1
!			write(11,*)j*dx*1e6,k*dy*1e6,fair(j,k),faii(j,k)
!		end do
!			write(11,*)
!	end do
!	close(11)
!	!输出修正的电场分布
!	open(10,file='exy.txt')
!	do j=1,xnum
!		do k=1,ynum
!			write(10,*)j*dx*1e6,k*dy*1e6,fdex(j,k),fdey(j,k),sqrt(fdex(j,k)**2+fdey(j,k)**2)
!		end do
!			write(10,*)
!	end do
!	close(10)
return
end subroutine


subroutine error()
	implicit none
	include 'rcommon.h'
	integer i,j
	real*8 gerror
	call sum_row()
	gerror=0d0
	do i=2,xnum-1
		do j=2,ynum-1
			if(row(i,j)/=0d0)then
	gerror=gerror+(((fdex(i,j)-fdex(i-1,j))/dx+(fdey(i,j)-fdey(i,j-1))/dy)*ebsn)/row(i,j)-1
			end if
		end do
	end do
	pois_error=abs(gerror)
	open(111,file='error.txt',position='append')
		write(111,*)step,gerror,snumi1-sum(iconi1),snume1-sum(icone1),snumi1-sum(iconi1)+snume1-sum(icone1)
	close(111,status='keep')
return
end subroutine







!说明：数据定时输出
subroutine check()
	implicit none
	include 'rcommon.h'
	integer im,it
	real*8 ratio
	ratio=dble(cki)/dble(ck)
	im=bstep+rcdsteps*ratio	!comput the step when to record
	if(cki<=ck.and.step==im)then
		write(outname,'(i3)')cki	!把这个百分比写到字符串中
		outname=adjustl(outname)	!adjustl将字符串里左边的空格清除,
		!打开保存时间的文件,timelist
		timeout=btime+cki*gtime
		open(666,file='timelist',position='append')
		!把时刻写进文件，保留两位小数，单位fs
		write(666,'(F6.2)')timeout
		close(666)
		!计算振幅
	!	call amplitude()

		!下面的一些子程序，将完成各种数据的输出
		!!!!!!!!!!!!!!!!!!!!!!输出为ROOT格式!!!!!!!!!!!!!!!!!!!!
		if(filetype==0.or.filetype==2)then
			!靶信息输出
			call tgtout()
			!场信息输出
			call fieldao()
		end if
		!!!!!!!!!!!!!!!!!!!!!!输出为TXT格式!!!!!!!!!!!!!!!!!!!!
		if(filetype>=1)then
			!靶信息输出
			call tgtout_txt()
			!场信息输出
			call fieldout_txt()
		end if
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		!其它一些检验信息输出，为txt格式
	!	call otherout()
		cki=cki+1	!表示一次数据保存完成，就要自加1，为了计算im，就是下次该要
					!到第几个时间步长的时候再次保存数据
	end if
	call trace()
return
end subroutine

!计算振幅
subroutine amplitude()
	implicit none
	include 'rcommon.h'
	integer ip,it

	ip=ynum/2
	do it=1,xnum
		laserao(1,it)=fdex(it,ip+1)*fdtrans
		laserao(2,it)=(fdey(it,ip)+fdey(it,ip+1))*0.5d0*fdtrans
		laserao(3,it)=fdez(it,ip)*fdtrans
		laserao(4,it)=sqrt(laserao(1,it)**2+laserao(2,it)**2)
		laserao(5,it)=sqrt(laserao(2,it)**2+laserao(3,it)**2)
		laserao(6,it)=sqrt(laserao(3,it)**2+laserao(4,it)**2)
	end do
return
end subroutine



!ROOT格式的粒子信息的输出
subroutine tgtout()
	implicit none
	include 'rcommon.h'
	if(tgt1==1)then
		call part2root(0,cki,timeout,1,1,&
				cnume1,xye1*1d6,vxye1,pxye1,enge1,ange1,gamae1,icone1)
		call part2root(1,cki,timeout,1,2,&
				cnumi1,xyi1*1d6,vxyi1,pxyi1,engi1,angi1,gamai1,iconi1)
	endif
	if(tgt2==1)then
		if(tgt1==1)then
			call part2root(1,cki,timeout,2,1,&
					cnume2,xye2*1d6,vxye2,pxye2,enge2,ange2,gamae2,icone2)
		else
			call part2root(0,cki,timeout,2,1,&
					cnume2,xye2*1d6,vxye2,pxye2,enge2,ange2,gamae2,icone2)
		end if
		call part2root(1,cki,timeout,2,2,&
				cnumi2,xyi2*1d6,vxyi2,pxyi2,engi2,angi2,gamai2,iconi2)
	endif
return
end subroutine


!ROOT格式的电磁场和激光信息的输出
subroutine fieldao()
	implicit none
	include 'rcommon.h'
	integer it,ip,k
	integer i,j
	real*8 xtics(xnum+1),ytics(ynum+1),positt
	real*8 fieldex(xnum+1),fieldey(xnum+1),fieldbz(xnum+1)
	real*8 fieldbx(xnum+1),fieldby(xnum+1),fieldez(xnum+1)
	real*8 allex(ijnum),alley(ijnum),allbz(ijnum)
	real*8 allbx(ijnum),allby(ijnum),allez(ijnum)
	do it=1,xnum+1
		xtics(it)=(it-1)*dx*1d6
	end do
	do it=1,ynum+1
		ytics(it)=(it-1)*dy*1d6
	end do
	call average() !把半格点上的场平均到整格点
	!the middle fields
	ip=int(ynum*posit1)+1
	fieldex(:)=fdex1(:,ip)*fdtrans
	fieldey(:)=fdey1(:,ip)*fdtrans
	fieldbz(:)=fdbz1(:,ip)*fdtransb
	fieldbx(:)=fdbx1(:,ip)*fdtransb
	fieldby(:)=fdby1(:,ip)*fdtransb
	fieldez(:)=fdez(:,ip)*fdtrans
	!all the fields
	do j=1,ynum+1
		do i=1,xnum+1
			k=(j-1)*(xnum+1)+i
			allex(k)=fdex1(i,j)*fdtrans
			alley(k)=fdey1(i,j)*fdtrans
			allbz(k)=fdbz1(i,j)*fdtransb
			allbx(k)=fdbx1(i,j)*fdtransb
			allby(k)=fdby1(i,j)*fdtransb
			allez(k)=fdez(i,j)*fdtrans
		end do
	end do
	call field2root(cki,timeout,xnum+1,ynum+1,xtics,ytics,&
				fieldex,fieldey,fieldbz,fieldbx,fieldby,fieldez,&
				laserao,allex,alley,allbz,allbx,allby,allez)
return
end subroutine


!TXT格式的粒子信息输出
subroutine tgtout_txt()
	implicit none
	include 'rcommon.h'
	integer it
	!如果靶1存在
	if(tgt1==1)then
		!新建一个TXT文件，文件名为 1i数字.txt = 靶1 + 离子 + 百分比 + .txt
		!trim的作用是清除字符串右边的空格
		open(61,file='1i'//trim(outname)//'.txt')
		!循环靶1的每个离子，输出离子的坐标，速度，动量，能量，相对论因子
		do it=1,cnumi1
			write(61,*)xyi1(1,it)*1d6,xyi1(2,it)*1d6,xyi1(3,it)*1d6,&
					vxyi1(1,it),vxyi1(2,it),vxyi1(3,it),vxyi1(4,it),&
					pxyi1(1,it),pxyi1(2,it),pxyi1(3,it),pxyi1(4,it),&
					engi1(it),gamai1(it)
		end do
		!新建一个TXT文件，文件名为 1e数字.txt = 靶1 + 电子 + 百分比 + .txt
		open(62,file='1e'//trim(outname)//'.txt')
		!循环靶1的每个电子，输出电子的坐标，速度，动量，能量，相对论因子
		do it=1,cnume1
			write(62,*)xye1(1,it)*1d6,xye1(2,it)*1d6,xye1(3,it)*1d6,&
					vxye1(1,it),vxye1(2,it),vxye1(3,it),vxye1(4,it),&
					pxye1(1,it),pxye1(2,it),pxye1(3,it),pxye1(4,it),&
					enge1(it),gamae1(it)
		end do
	end if
	!如果靶2存在
	if(tgt2==1)then
		open(63,file='2i'//trim(outname)//'.txt')
		do it=1,cnumi2
			write(63,*)xyi2(1,it)*1d6,xyi2(2,it)*1d6,xyi2(3,it)*1d6,&
					vxyi2(1,it),vxyi2(2,it),vxyi2(3,it),vxyi2(4,it),&
					pxyi2(1,it),pxyi2(2,it),pxyi2(3,it),pxyi2(4,it),&
					engi2(it),gamai2(it)
		end do
		open(64,file='2e'//trim(outname)//'.txt')
		do it=1,cnume2
			write(64,*)xye2(1,it)*1d6,xye2(2,it)*1d6,xye2(3,it)*1d6,&
					vxye2(1,it),vxye2(2,it),vxye2(3,it),vxye2(4,it),&
					pxye2(1,it),pxye2(2,it),pxye2(3,it),pxye2(4,it),&
					enge2(it),gamae2(it)
		end do
	end if
return
end subroutine

!TXT格式的电磁场信息的输出
subroutine fieldout_txt()
	implicit none
	include 'rcommon.h'
	integer i,j
	!如果计算了TE
	if(teonf==1)then
		open(66,file='fdex'//trim(outname)//'.txt')
		open(67,file='fdey'//trim(outname)//'.txt')
		open(68,file='fdbz'//trim(outname)//'.txt')
		do i=1,xnum
			do j=1,ynum+1
				write(66,*)i,j,fdex(i,j)
			end do
			write(66,*)
		end do
		do i=1,xnum+1
			do j=1,ynum
				write(67,*)i,j,fdey(i,j)
			end do
			write(67,*)
		end do
		do i=1,xnum
			do j=1,ynum
				write(68,*)i,j,fdbz(i,j)
			end do
			write(68,*)
		end do
		close(66)
		close(67)
		close(68)
	end if


	!TM
	if(tmonf==1)then
		open(166,file='fdbx'//trim(outname)//'.txt')
		open(167,file='fdby'//trim(outname)//'.txt')
		open(168,file='fdez'//trim(outname)//'.txt')
		do i=1,xnum+1
			do j=1,ynum
				write(166,*)i,j,fdbx(i,j)
			end do
			write(166,*)
		end do
		do i=1,xnum
			do j=1,ynum+1
				write(167,*)i,j,fdby(i,j)
			end do
			write(167,*)
		end do
		do i=1,xnum+1
			do j=1,ynum+1
				write(168,*)i,j,fdez(i,j)
			end do
			write(168,*)
		end do
		close(166)
		close(167)
		close(168)
	end if

return
end subroutine












!一些程序检验信息的输出，TXT格式
subroutine otherout()
	implicit none
	include 'rcommon.h'
	integer i,j
	real*8 nq,temp1,temp2,temp3
	temp1=sum(cutx)
	temp2=sum(cuty)
	temp3=sum(row)

	!检验电流密度
	!每一时刻都把数据覆盖更新
	open(777,file='cute'//trim(outname)//'.txt')
	open(771,file='cuti'//trim(outname)//'.txt')
	open(772,file='cut'//trim(outname)//'.txt')
	do i=1,xnum
		do j=1,ynum
			if(cutxe1(i,j)/=0)then
		!		write(777,*)i,j,cutxe2(i,j),cutye2(i,j)
			end if
			if(cutxi1(i,j)/=0)then
		!		write(771,*)i,j,cutxi2(i,j),cutyi2(i,j)
			end if
		!	if(cutx(i,j)/=0)then
				write(772,*)i,j,cutx(i,j),cuty(i,j),row(i,j),temp1,temp2,temp3
		!	end if
		end do
	end do
	close(771)
	close(772)
	close(777)
return
end subroutine

!检验指定粒子的运动轨迹
subroutine trace()
	implicit none
	include 'rcommon.h'
	integer n1,n2,n3,n4,n5
	real*8 rd
	call random_seed()
	if(tgt1>0)then
		call random_number(rd)
		n1=floor(rd*snume1)+1
		call random_number(rd)
		n2=floor(rd*snume1)+1
		call random_number(rd)
		n3=floor(rd*snume1)+1
		call random_number(rd)
		n4=floor(rd*snume1)+1
		call random_number(rd)
		n5=floor(rd*snume1)+1
		open(111,file='trace_e.txt',position='append')
		write(111,*)rtime*1e15,xye1(1,n1)*1e6,xye1(2,n1)*1e6,xye1(3,n1)*1e6,&
								xye1(1,n2)*1e6,xye1(2,n2)*1e6,xye1(3,n2)*1e6,&
								xye1(1,n3)*1e6,xye1(2,n3)*1e6,xye1(3,n3)*1e6,&
								xye1(1,n4)*1e6,xye1(2,n4)*1e6,xye1(3,n4)*1e6,&
								xye1(1,n5)*1e6,xye1(2,n5)*1e6,xye1(3,n5)*1e6
		close(111,status='keep')
		call random_number(rd)
		n1=floor(rd*snumi1)+1
		call random_number(rd)
		n2=floor(rd*snumi1)+1
		call random_number(rd)
		n3=floor(rd*snumi1)+1
		call random_number(rd)
		n4=floor(rd*snumi1)+1
		call random_number(rd)
		n5=floor(rd*snumi1)+1
		open(222,file='trace_i.txt',position='append')
		write(222,*)rtime*1e15,xyi1(1,n1)*1e6,xyi1(2,n1)*1e6,xyi1(3,n1)*1e6,&
								xyi1(1,n2)*1e6,xyi1(2,n2)*1e6,xyi1(3,n2)*1e6,&
								xyi1(1,n3)*1e6,xyi1(2,n3)*1e6,xyi1(3,n3)*1e6,&
								xyi1(1,n4)*1e6,xyi1(2,n4)*1e6,xyi1(3,n4)*1e6,&
								xyi1(1,n5)*1e6,xyi1(2,n5)*1e6,xyi1(3,n5)*1e6
		close(222,status='keep')
	end if
return
end subroutine






!自定义函数:简单的高斯计算
Function gauss(gt1,gt2,gs)
	implicit none
	include 'rcommon.h'
	real*8 gauss
	integer gs
	real*8 gt1,gt2
	gauss=exp(-1d0*(gt1/gt2)**gs)
return
end function

!自定义函数:通过传递过来的横坐标值，判断是哪个靶，是否在靶的区域内
Function gradient(gx)
	implicit none
	include 'rcommon.h'
	real*8 gradient
	real*8 gx
	real*8,external::grad_tgt
	gradient=0.
	if(tgt1==1.and.gx>=tgtmx1.and.gx<=tgtmx1+tgtx1)then
		gradient=grad_tgt(gx,grad1,grshpf1,grshpr1,grsta1,grend1,tgtx1,tgtmx1)
	end if
	if(tgt2==1.and.gx>=tgtmx2.and.gx<=tgtmx2+tgtx2)then
		gradient=grad_tgt(gx,grad2,grshpf2,grshpr2,grsta2,grend2,tgtx2,tgtmx2)
	end if
return
end function

!自定义函数:用来判断密度梯度的位置，前表面，后表面，还是前后表面都有
Function grad_tgt(gx,grad,grshpf,grshpr,grsta,grend,tgtx,tgtmx)
	implicit none
	include 'rcommon.h'
	integer grad,grshpf,grshpr
	real*8 grad_tgt
	real*8,external::grad_shape
	real*8 tempg1,tempg2,tempg3
	real*8 gx,grsta,grend,tgtx,tgtmx
	!front gradient length
	tempg1=tgtx*grsta
	!rear gradient length
	tempg2=tgtx*grend
	!rear surface position
	tempg3=tgtx+tgtmx
	if(gx>=tgtmx.and.gx<=tgtmx+tgtx)then
		select case(grad)
			!没有梯度
			case(0)
				grad_tgt=1d0
			!前表面梯度
			case(1)
				if(gx>=tgtmx.and.gx<=tgtmx+tempg1)then
					grad_tgt=grad_shape(gx-tgtmx,tempg1,grshpf)
				else
					grad_tgt=1d0
				end if
			!后表面梯度
			case(2)
				if(gx>=tempg3-tempg2.and.gx<=tempg3)then
					grad_tgt=grad_shape(tempg3-gx,tempg2,grshpr)
				else
					grad_tgt=1d0
				end if
			!前后表面梯度
			case(3)
				if(gx>=tgtmx.and.gx<=tgtmx+tempg1)then
					grad_tgt=grad_shape(gx-tgtmx,tempg1,grshpf)
				else if(gx>=tempg3-tempg2.and.gx<=tempg3)then
					grad_tgt=grad_shape(tempg3-gx,tempg2,grshpr)
				else
					grad_tgt=1d0
				end if
		end select
	else
		grad_tgt=0d0
	end if
return
end function

!自定义函数:用来计算各种不同密度梯度形状的权重因子
Function grad_shape(grs1,grs2,grshp)
	implicit none
	include 'rcommon.h'
	integer grshp
	real*8 grad_shape,grs1,grs2
	select case(grshp)
		!线性梯度
		case(0)
			grad_shape=grs1/grs2
		!高斯梯度
		case(1)
			grad_shape=exp(-1d0*pi*((grs1-grs2)/grs2)**2)
		!正弦梯度
		case(2)
			grad_shape=sin(pi/2d0*grs1/grs2)
	end select
return
end function

!自定义函数:把两个4*4矩阵对应相乘并求和
Function array(arr1,arr2)
	implicit none
	include 'rcommon.h'
	real*8 array
	real*8 arr1(4),arr2(4)
	array=arr1(1)*arr2(1)+arr1(2)*arr2(2)+arr1(3)*arr2(3)+arr1(4)*arr2(4)
return
end function
