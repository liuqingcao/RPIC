	
	set terminal postscript eps color solid

	set xtics nomirror
	set ytics nomirror
	set x2tics
	set y2tics

	set xtics font "times.ttf,20"
	set ytics font "times.ttf,20"
	set x2tics font "times.ttf,20"
	set y2tics font "times.ttf,20"

	set xlabel 't/fs' font "times.ttf,20"
	set ylabel 'E' font "times.ttf,20"
	set x2label 'X' font "times.ttf,20"
	set y2label 'Y' font "times.ttf,20"

	set x2tics  tc lt 2
	set y2tics  tc lt 2
	
	set x2label  tc lt 2
	set y2label  tc lt 2

	set key right
	set xrange[0:68]
	set x2range[0:20]
	set output 'init.eps'

	plot "./laser.txt" u 1:2 w l lc 1 lw 3, \
		"./laser.txt" u 1:3 w l lc 3 lw 3, \
		"./laser.txt" u 1:4 w l lc 5 lw 3,\
		"./tgt1.txt" w p lc 2   axes x2y2

