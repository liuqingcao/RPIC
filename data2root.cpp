/***********************************************************************
 * 程序名称：data2root.cpp
 * 作者：	**
 * 程序功能：此代码以子函数的形式被rpic.f90程序中的数据输出部分调用，用于把不同时
 * 			刻的粒子信息和电磁场等数据保存成root文件（tree结构）的格式
 * 使用说明：在此文件所在文件夹中打开终端，输入 g++ -c data2root.cpp 
 * 		-I`root-config --incdir` `root-config --glibs` `root-config --libs`
 * 			会生成 data2root.o 文件。（具体见 auto_rpic.sh 中说明）
************************************************************************/

#include <iostream>	//C++里面处理输入输出要使用的头文件
#include <fstream>	//读写文件流处理
#include <string>	//处理字符串
#include <sstream>
#include <stdlib.h>	//
#include <TFile.h>	//处理ROOT文件
#include <TTree.h>	//处理TREE
#include <TBranch.h>//处理TREE中的Branch
#include <TString.h>//
#include <ctime>	//计时
using namespace std;//全局的命名空间


double start,end,TIME;
/* 
 * 
*/
extern "C" void part2root_(int *flag,int *record,double *rtime,int *tgt,int *type,
					int *cnum,double xy[][3],double vxy[][4],double pxy[][4],
					double eng[],double ang[],double gama[],int icon[])
{
	//定义一些参量
	double Time,X,Y,Z,V,Vx,Vy,Vz,P,Px,Py,Pz,Energy,Angle,Gama;
	int Target,Type,ID,Icon;
	//定义一些指针
	TFile *f;	//ROOT文件指针
	TTree *t1;	//TREE的指针
	start=clock();	
	//字符串拼接
	stringstream Str_record;
	Str_record<<*record;
	string Number = Str_record.str();
	string First = "particle_";
	string Last = ".root";		
	string FileName = First + Number + Last;	

	//如果是一个新的时刻,则新建ROOT文件，定义Branch
	//如果是一个旧的时刻,则打开ROOT文件，定位Branch
	if(*flag==0)
	{
		//creat a new ROOT file
		f= new TFile(FileName.c_str(),"recreate");//新建文件
		t1= new TTree("t1","Particles");//新建TREE
		//新建BRANCH，定义名称和数据类型
		t1->Branch("Time",&Time,"Time/D");//记录时间
		t1->Branch("Target",&Target,"Target/I");//靶号
		t1->Branch("Type",&Type,"Type/I");//粒子种类		
		t1->Branch("ID",&ID,"ID/I");//粒子序号，可以给每个电子编个号
		t1->Branch("X",&X,"X/D");//位置X
		t1->Branch("Y",&Y,"Y/D");//位置Y
		t1->Branch("Z",&Z,"Z/D");//位置Z
		t1->Branch("P",&P,"P/D");//动量P
		t1->Branch("Px",&Px,"Px/D");//动量Px
		t1->Branch("Py",&Py,"Py/D");//动量Py
		t1->Branch("Pz",&Pz,"Pz/D");//动量Pz
		t1->Branch("V",&V,"V/D");//速度V
		t1->Branch("Vx",&Vx,"Vx/D");//速度Vx
		t1->Branch("Vy",&Vy,"Vy/D");//速度Vy
		t1->Branch("Vz",&Vz,"Vz/D");//速度Vz
		t1->Branch("Energy",&Energy,"Energy/D");//能量Energy
		t1->Branch("Angle",&Angle,"Angle/D");//偏转角度,Angle
		t1->Branch("Gama",&Gama,"Gama/D");//相对论因子
		t1->Branch("Icon",&Icon,"Icon/I");//出界标记,Icon
	}
	else if(*flag==1)
	{
		f = new TFile(FileName.c_str(),"update");//使用UPDATE连续写入
		t1 = (TTree*)f->Get("t1");//读取TREE
		//指定BRANCH地址
		t1->SetBranchAddress("Time",&Time);
		t1->SetBranchAddress("Target",&Target);
		t1->SetBranchAddress("Type",&Type);
		t1->SetBranchAddress("ID",&ID);
		t1->SetBranchAddress("X",&X);
		t1->SetBranchAddress("Y",&Y);
		t1->SetBranchAddress("Z",&Z);
		t1->SetBranchAddress("P",&P);
		t1->SetBranchAddress("Px",&Px);
		t1->SetBranchAddress("Py",&Py);
		t1->SetBranchAddress("Pz",&Pz);
		t1->SetBranchAddress("V",&V);
		t1->SetBranchAddress("Vx",&Vx);
		t1->SetBranchAddress("Vy",&Vy);
		t1->SetBranchAddress("Vz",&Vz);
		t1->SetBranchAddress("Energy",&Energy);
		t1->SetBranchAddress("Angle",&Angle);
		t1->SetBranchAddress("Gama",&Gama);
		t1->SetBranchAddress("Icon",&Icon);		
	}
	
	//开始循环，把各种信息取出并写入Branch
			Time=*rtime;
			Target=*tgt;
			Type=*type;		
	for(int i=0;i<=*cnum-1;i++)
		{
			ID=i;
			X=xy[i][0];
			Y=xy[i][1];
			Z=xy[i][2];
			V=vxy[i][0];
			Vx=vxy[i][1];
			Vy=vxy[i][2];
			Vz=vxy[i][3];
			P=pxy[i][0];
			Px=pxy[i][1];
			Py=pxy[i][2];
			Pz=pxy[i][3];
			Energy=eng[i];
			Angle=ang[i];
			Gama=gama[i]-1;
			Icon=icon[i];
			t1->Fill();//写入数据
		}
		t1->Write("", TObject::kOverwrite);//把数据从内存写到文件，同时保存最新的数据
		f->Close();	//关闭ROOT文件
		delete f;	//删除文件指针
		end=clock();//记录cpu时间当作运行此段代码的结束时间
		TIME=(end-start)/CLOCKS_PER_SEC;//单位S
	//	cout << "time1:" << TIME << endl;		
}

/* 
 * 
*/
extern "C" void field2root_(int *record,double *rtime,int *xnum,int *ynum,
						 double xtics[],double ytics[],
	double fieldex[],double fieldey[],double fieldbz[],double fieldbx[],double fieldby[],double fieldez[],			
	double laserao[][6],double allex[],double alley[],double allbz[],double allbx[],double allby[],double allez[])
{
	double Time,X,Y,XX,YY,Ax,Ay,Az,Axy,Ayz,Axyz,AEx,AEy,ABz,ABx,ABy,AEz;
	double Ex,Ey,Bz,Bx,By,Ez;
	TFile *f;
	TTree *t2, *t3;
	start=clock();
	{	
		stringstream Str_record;
		Str_record<<*record;
		string Number = Str_record.str();
		string First = "field_";
		string Last = ".root";		
		string FileName = First + Number + Last;
		//creat a new ROOT file
		f= new TFile(FileName.c_str(),"recreate");//新建文件
		t2= new TTree("t2","Field1");//新建TREE
		t3= new TTree("t3","Field2");
		//新建BRANCH，定义名称和数据类型
		t2->Branch("Time",&Time,"Time/D");//记录时间
		t2->Branch("X",&X,"X/D");//
		t2->Branch("Y",&Y,"Y/D");//	
		t2->Branch("Ex",&Ex,"Ex/D");//
		t2->Branch("Ey",&Ey,"Ey/D");//
		t2->Branch("Bz",&Bz,"Bz/D");//
		t2->Branch("Bx",&Bx,"Bx/D");//
		t2->Branch("By",&By,"By/D");//
		t2->Branch("Ez",&Ez,"Ez/D");//
		
		t2->Branch("Ax",&Ax,"Ax/D");//
		t2->Branch("Ay",&Ay,"Ay/D");//
		t2->Branch("Az",&Az,"Az/D");//
		t2->Branch("Axy",&Axy,"Axy/D");//
		t2->Branch("Ayz",&Ayz,"Ayz/D");//
		t2->Branch("Axyz",&Axyz,"Axyz/D");//
		
		
		
		t3->Branch("Time",&Time,"Time/D");//记录时间
		t3->Branch("XX",&XX,"XX/D");//
		t3->Branch("YY",&YY,"YY/D");//				
		t3->Branch("AEx",&AEx,"AEx/D");//
		t3->Branch("AEy",&AEy,"AEy/D");//
		t3->Branch("ABz",&ABz,"ABz/D");//		
		t3->Branch("ABx",&ABx,"ABx/D");//		
		t3->Branch("ABy",&ABy,"ABy/D");//		
		t3->Branch("AEz",&AEz,"AEz/D");//		
	}
	Time=*rtime;
	for(int i=0;i<*xnum;i++)//循环数组
		{
			X=xtics[i]; 
			Ex=fieldex[i];
			Ey=fieldey[i];
			Bx=fieldbx[i];						
			Bz=fieldbz[i];
			By=fieldby[i];
			Ez=fieldez[i]; 					
			Ax=laserao[i][0];
			Ay=laserao[i][1];
			Az=laserao[i][2];
			Axy=laserao[i][3];
			Ayz=laserao[i][4];
			Axyz=laserao[i][5];
			t2->Fill();//写入数据
		}
	t2->Write();//把数据从内存写到文件	
	for(int i=0;i<*xnum;i++){	
		for(int j=0;j<*ynum;j++){
			XX=xtics[i];
			YY=ytics[j];
			int allip = (j)*(*xnum)+i ;	
			AEx=allex[allip];
			AEy=alley[allip];
			ABz=allbz[allip];			
			ABx=allbx[allip];			
			ABy=allby[allip];
			AEz=allez[allip];						
			t3->Fill();	
		}
	}
	t3->Write();
	f->Close();
	delete f;
	end=clock();//记录cpu时间当作运行此段代码的结束时间
	TIME=(end-start)/CLOCKS_PER_SEC;//单位S
//	cout << "time2:" << TIME << endl;	
}
