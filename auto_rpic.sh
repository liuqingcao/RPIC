#!/bin/bash
#说明：
#程序功能：次程序的作用是自动编译并运行程序，如果头文件（rcommon.h）中需要计算的靶粒
#	子的个数不正确，则会进行自动修正，解决了以前需要手动修正的问题
#使用方法：把次文件和程序文件放到一个文件夹，修改好头文件中的一些计算条件后在此文件夹
#	中打开终端，输入 ./auto_rpic.sh （如果是第一次使用本文件或者是修改本文件后需要
#	先赋予此文件可以运行的属性，输入 chmod +x auto_rpic.sh ,以后就可以直接运行了）
#

	whie=$(sed -n '/!ID,DO NOT DELETE/=' rcommon.h)
	icon=`sed -n ${whie}p rcommon.h`
	if [ "$icon" != "!ID,DO NOT DELETE" ]; then
		echo "头文件行号不匹配!"
		exit
	fi
	whi1=$[ $whie + 3 ]
	whe1=$[ $whie + 4 ]
	whi2=$[ $whie + 5 ]
	whe2=$[ $whie + 6 ]
	#echo ${whi1}  ${whe1}  ${whi2}  ${whe2}  ${whi3}

#删除一些可能存在的文件
	rm -f *.eps *.gif *.o *.run
	rm -f *.root *.txt temp*
	rm -f timelist pipe



	sed -e  "${whi1}c\  snumi1=        1,&!靶-1 离子个数"  \
		-e  "${whe1}c\  snume1=        1,&!靶-1 电子个数" 	 \
		-e  "${whi2}c\  snumi2=        1,&!靶-2 离子个数"   \
		-e  "${whe2}c\  snume2=        1 &!靶-2 电子个数"  rcommon.h -i

#程序编译和运行
	#编译f90文件，程序源代码主体部分，编译后生成rpic.o文件-fbounds-check
	gfortran -c rpic.f90 -w\
				-mcmodel=medium	-fbounds-check -I`root-config --incdir` `root-config --glibs` `root-config --libs`

	#编译cpp文件，处理数据输出成ROOT文件的子程序部分，用C++写的，编译后生成data2root.o文件
	g++ -c data2root.cpp -I`root-config --incdir` `root-config --glibs` `root-config --libs`
	#编译成可执行文件，把上面编译好的两个.O后缀的文件一起编译生成rpic.run，是可执行文件
	gfortran rpic.o data2root.o -w\
				-mcmodel=medium	-fbounds-check
				-I`root-config --incdir` `root-config --glibs` `root-config --libs` \
				-o rpic.run
	#在终端中显示下面的文字
	echo "开始运行程序"
	#运行，运行程序，运行上面生成的可执行文件
	./rpic.run


#说明：程序开始运行后，会检测头文件（rcommon.h）里计算粒子的个数是否正确，程序会生
#	成文件temp1和temp2（分别对应两个靶，文件里存放的是正确的计算粒子的个数），并会
#	停止下来，执行下面的命令，修改头文件（rcommon.h）里计算粒子的个数。
#	如果计算中没用靶则不会有这两个文件生成，自然也没有计算粒子个数判断这个步骤了。


#判断temp1文件是否存在
if [ -e temp1 ];then
	#读取temp1文件第1行
	numi1=`sed -n '1 p' temp1`
	#读取temp1文件第2行
	nume1=`sed -n '2 p' temp1`
	#读取temp1文件第3行，写的是“right”或者是“wrong”，用来判断粒子个数是否正确
	icon1=`sed -n '3 p' temp1`
	#若读取到的为"wrong"，则开始修改头文件，若是“right”，则不执行这段代码
	if [ ${icon1} == "wrong" ];then
		#修改头文件rcommon.h中的51，52行，把正确的计算粒子个数写进去一定要保持
		#头文件的行数结构不变，这个修改只会判断文件行数
		sed -e  "${whi1}c\ ${numi1},&!靶-1 离子个数" \
			-e  "${whe1}c\ ${nume1},&!靶-1 电子个数" \
		rcommon.h -i
		#编译rpic.f90文件，因为头文件改变了，所以要重新编译成.O文件
		gfortran -c rpic.f90 -w\
				-mcmodel=medium	-fbounds-check \
				-pipe -O3 -ftracer -fivopts -ftree-loop-linear \
				-ftree-vectorize -fforce-addr -fomit-frame-pointer
		#把.O文件编译成可执行文件
	gfortran rpic.o data2root.o -w\
				-mcmodel=medium	-fbounds-check \
				-pipe -O3 -ftracer -fivopts -ftree-loop-linear \
				-ftree-vectorize -fforce-addr -fomit-frame-pointer \
				-I`root-config --incdir` `root-config --glibs` `root-config --libs` \
				-o rpic1.run
		#运行程序
		echo "更正靶1粒子个数：运行程序"
		./rpic1.run
	fi
fi


#说明：靶1修改完成后，运行程序，如果靶2的计算粒子个数不正确，则程序再次停止，并进行如上
#	同样的修正，不同的是粒子个数放在temp2文件里，修改的是第53、54行


#判断temp2文件是否存在
if [ -e temp2 ];then
	#取第1行
	numi2=`sed -n '1 p' temp2`
	#取第2行
	nume2=`sed -n '2 p' temp2`
	#读取第3行
	icon2=`sed -n '3 p' temp2`
	#若读取到的为"wrong"
	#echo $icon2
	if [ ${icon2} == "wrong" ];then
		#修改rcommon.h中的53，54行
		sed -e  "${whi2}c\ ${numi2},&!靶-2 离子个数" \
			-e  "${whe2}c\ ${nume2},&!靶-2 电子个数" \
		rcommon.h -i
		#编译f90
		gfortran -c rpic.f90 -w\
				-mcmodel=medium	-fbounds-check \
				-pipe -O3 -ftracer -fivopts -ftree-loop-linear \
				-ftree-vectorize -fforce-addr -fomit-frame-pointer
		#编译可执行文件
		gfortran rpic.o data2root.o -w\
				-mcmodel=medium	-fbounds-check \
				-pipe -O3 -ftracer -fivopts -ftree-loop-linear \
				-ftree-vectorize -fforce-addr -fomit-frame-pointer \
				-I`root-config --incdir` `root-config --glibs` `root-config --libs` \
				-o rpic2.run
		#运行程序
		echo "更正靶2粒子个数：运行程序"
		./rpic2.run
	fi
fi



#删除一些文件
	rm -f *.eps *.o *.run temp*
#删除旧的文件夹,新建文件夹
	mkdir -p txt  ;rm -f ./txt/*  ;mv *.txt ./txt
	mkdir -p root ;rm -f ./root/* ;mv *.root ./root
#处理root数据
	root root2pic.cpp -b -l -q
#备份头文件到out_gif文件夹
	cp rcommon.h ./out_gif/rcommon_backup.h
#

