/*
 * 
*/
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <TMath.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TString.h>
#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TSpectrum.h>
using namespace std;


//靶的个数，
int TGT_1,TGT_2,TGT_3,TGT_S ;
//电子信息，
bool ELE = 1 ;
//离子信息，
bool ION = 1 ;
//电磁场信息，
bool FLD = 1 ;
//测试用，只做一个时刻的数据图
bool TestFlag = 0 ;
//指定作哪个时刻的数据图
int TestOne = 20 ;
//Canvas的大小 
int WPixel = 1024;
int HPixel = 768;
//输出文件保存格式
string SaveType = ".gif";
//输出文件目录
string OUTPATH = "./out_gif/" ; 
//时间和计算区域大小的文件的名称
char *TimeFile = "timelist";
char *LongFile = "pipe";
//root文件存放的目录
string PATH = "./root/";
//定义一些Hist的参数
Double_t X_LOW = 0 ;
Double_t X_UP;
Int_t X_BIN = 1000 ;

Double_t Y_LOW = 0 ;
Double_t Y_UP;
Int_t Y_BIN = 1000 ;

Double_t E_MIN = 0 ;
Double_t E_MAX = 3000 ;
Int_t E_BIN = 100 ;

Double_t A_MIN = -180 ;
Double_t A_MAX = 180 ;
Int_t A_BIN = 360 ;

void root2pic()
{
	//删除已经存在的数据 
	system("mkdir -p out_gif; rm -f ./out_gif/*;");
	//ROOT全局设置
	SetStyle();
	//保存计算尺寸的文件
	FILE *WL=fopen(LongFile,"r");	
		fscanf(WL,"%d %d %d %d %lf %lf",&TGT_1,&TGT_2,&TGT_3,&TGT_S,&X_UP,&Y_UP);
		//cout<<TGT_1<<TGT_2<<TGT_3<<X_UP<<Y_UP<<endl;
	fclose(WL);
	//保存时间信息的文件
	FILE *list=fopen(TimeFile,"r");
		int lines=CountLines(TimeFile);//计算timelist文件的行数
		cout<<"List number： "<<lines<<endl;
		//按行获取数据，只需要把第一列的时间存储在Time数组里
		float TimeList[1000];
		for(int i=0;i<1000;i++)	TimeList[i]=0;
		for(int i=0;i<lines;i++) fscanf(list,"%f",&TimeList[i]);
	fclose(list);	
	//
	int startNO ,finishNO ;	
	if(TestFlag)
	{
		startNO = TestOne;
		finishNO = TestOne+1;		
	}
	else
	{
		startNO = 0;
		finishNO = lines;		
	}
	for(int i=startNO;i<finishNO;i++)
	{
		stringstream Str_record;
		Str_record << i;
		string Number = Str_record.str();
		string PFirst = "particle_";
		string FFirst = "field_";	
		string Last = ".root";		
		string PFileName = PATH + PFirst + Number + Last;
		string FFileName = PATH + FFirst + Number + Last;
	//open root files & get the trees
		//判断是否有Particle*文件
		bool flag = 0 ;		
		ifstream isfile(PFileName.c_str());
		if(isfile)	flag = 1 ;
		if(flag)
		{
			TFile *pfile = new TFile(PFileName.c_str());
			TTree *ptree = (TTree*)pfile->Get("t1");
		}			
		TFile *ffile = new TFile(FFileName.c_str());	
		TTree *ftree1 = (TTree*)ffile->Get("t2");
		TTree *ftree2 = (TTree*)ffile->Get("t3");
		//creat Canvas name	
		int target,type;
		float CurrentTime = TimeList[i] ;
		
		if(flag)
		{
			if(TGT_1==1)
			{	
				//Target-1
				target = 1;
				if(ELE)
				{
					//Electron	
					type = 1;
					ToDrawP(target,type,CurrentTime,ptree);
				}
				if(ION)
				{		
					//Ion	
					type = 2;
					ToDrawP(target,type,CurrentTime,ptree);
				}	
			}
			if(TGT_2==1)
			{	
				//Target-2:Electron	
				target = 2;			
				if(ELE)
				{
					//Electron	
					type = 1;
					ToDrawP(target,type,CurrentTime,ptree);
				}
				if(ION)
				{		
					//Ion	
					type = 2;
					ToDrawP(target,type,CurrentTime,ptree);
				}	
			}
			if(TGT_3==1)
			{
				
			}
			if(TGT_1==1&&TGT_2==1)
			{
				target = 0;
				if(ELE)
				{
					//all the Electron	
					type = 1;
					ToDrawP(target,type,CurrentTime,ptree);
				}
				if(ION && TGT_S==1)				
				{
					//all the ion
					type = 2;
					ToDrawP(target,type,CurrentTime,ptree);					
				}
			}
		}		
		if(FLD)
		{
			//Field and Amplitude
			ToDrawF(CurrentTime,ftree1,ftree2);	
			//Check(CurrentTime,ftree1,ftree2);	
		}
		//关闭ROOT文件 
		if(flag) pfile->Close();
		ffile->Close();		
	}

}


//
Int_t SetStyle()
{	
	gStyle->SetOptStat(0000);
	const int NRGBs = 7, NCont = 99;
	Double_t stops[NRGBs] = { 0.00, 0.04, 0.25, 0.45, 0.60, 0.75, 1.00 };
	Double_t red[NRGBs]   = { 1.00, 0.00, 0.00, 0.00, 0.97, 0.97, 0.10 };
	Double_t green[NRGBs] = { 1.00, 0.97, 0.30, 0.40, 0.97, 0.00, 0.00 };
	Double_t blue[NRGBs]  = { 1.00, 0.97, 0.97, 0.00, 0.00, 0.00, 0.00 };
	TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
	gStyle->SetNumberContours(NCont);	

}




//
Int_t ToDrawP(Int_t target,Int_t type,Float_t CTime,TTree *tree)
{	
	Int_t col;
	//particle is not OUT
		string OutIcon = "Icon==1";
	//Current Time CUT	
		stringstream Str_time;
		Str_time << CTime;
		string SCTime = Str_time.str();
		string timename = "Time==";
		string TimeCut = timename + SCTime;
	//Species CUT		
		stringstream Str_type;
		Str_type << type;
		string SCType = Str_type.str();
		string typeename = "Type==";
		string TypeCut = typeename + SCType;			
	//Target CUT	
		//both tgt1 and tgt2
		string TargetCut;
		if(target==0)
		{
			TargetCut = "Target==1||Target==2";
		}
		else
		{
			stringstream Str_target;
			Str_target << target;
			string SCTarget = Str_target.str();
			string targetname = "Target==";
			TargetCut = targetname + SCTarget;		
		}
	//Canvas NAME
		string First,Middle,Last;
		switch(target)
		{
			case 0 :
				First = "T12";
				break;
			case 1 :
				First = "T1";
				break;
			case 2 :
				First = "T2";
				break;				
			case 3 :
				First = "In";
				break;				
		}
		switch(type)
		{
			case 1 :
				Middle = 'ELE';				
				break;
			case 2 :
				Middle = 'Ion';
				break;
		}						
		Last = SCTime;
		string CanName = First + '-' + Middle + '-' + Last + "T";
	//get the max energy value
		Double_t SomeMax = tree->GetMaximum("Energy")*1.2;
		if(SomeMax==0.0)	SomeMax += 1e-10;
		E_MAX =  SomeMax ;
	//creat Canvas					
		TCanvas *PCanvas = new TCanvas(CanName.c_str(),CanName.c_str(),WPixel,HPixel);
		PCanvas->Divide(3,2);
		string CUT;
		
	//Enengy and Angle
		PCanvas->cd(1);
		gPad->SetGrid();	
		TH2F *h1 = new TH2F("h1","Angle:Energy",E_BIN,E_MIN,E_MAX,A_BIN,A_MIN,A_MAX);
		CUT = TargetCut + "&&" + TypeCut+ "&&" + OutIcon ;
		if(tree->Draw("Angle:Energy>>h1",CUT.c_str(),"colz")>0)		
		{
			h1->GetXaxis()->SetLabelSize(0.06);			
			h1->GetYaxis()->SetLabelSize(0.06);
			h1->GetXaxis()->SetTitleSize(0.05);	
			h1->GetYaxis()->SetTitleSize(0.05);	
		}
		h1->Clear();
	//Energy on each particle
		PCanvas->cd(2);
		gPad->SetGrid();	
		CUT = TargetCut + "&&" + TypeCut + "&&" + OutIcon ;
		if(tree->Draw("Y:X:Energy",CUT.c_str(),"colz")>0)
		{
			htemp->GetXaxis()->SetLabelSize(0.04);			
			htemp->GetYaxis()->SetLabelSize(0.06);
			htemp->GetXaxis()->SetTitleSize(0.05);	
			htemp->GetYaxis()->SetTitleSize(0.05);
			htemp->Clear();
		}				
	//Density	
		PCanvas->cd(3);
		gPad->SetGrid();
		TH2F *h3 = new TH2F("h3","Y:X",X_BIN,X_LOW,X_UP,Y_BIN,Y_LOW,Y_UP);		
		CUT = TargetCut + "&&" + TypeCut + "&&" + OutIcon ;
		if(tree->Draw("Y:X>>h3",CUT.c_str(),"colz")>0)
		{
			h3->GetXaxis()->SetLabelSize(0.06);			
			h3->GetYaxis()->SetLabelSize(0.06);
			h3->GetXaxis()->SetTitleSize(0.05);	
			h3->GetYaxis()->SetTitleSize(0.05);	
		}
		h3->Clear();	
	//Angle Spectrum
		PCanvas->cd(4);
		gPad->SetGrid();
		CUT = TargetCut + "&&" + TypeCut + "&&" + OutIcon;
		if(tree->Draw("Angle",CUT.c_str(),"")>0)
		{
			htemp->GetXaxis()->SetLabelSize(0.05);	
			htemp->GetYaxis()->SetLabelSize(0.04);
			htemp->GetXaxis()->SetTitleSize(0.05);	
			htemp->GetYaxis()->SetTitleSize(0.05);
			htemp->SetLineWidth(2);			
			col=4;
			htemp->SetLineColor(col);					
			htemp->GetXaxis()->SetTitleColor(col);
			htemp->GetYaxis()->SetTitleColor(col);
			htemp->Clear();
		}			
	//Phase
		PCanvas->cd(5);
		gPad->SetGrid();
		CUT = TargetCut + "&&" + TypeCut + "&&" + OutIcon;
		if(tree->Draw("Px:X",CUT.c_str(),"cont0")>0)	
		{
			htemp->GetXaxis()->SetLabelSize(0.05);			
			htemp->GetYaxis()->SetLabelSize(0.06);
			htemp->GetXaxis()->SetTitleSize(0.05);	
			htemp->GetYaxis()->SetTitleSize(0.05);
			htemp->Clear();
		}			
	//Energy Spectrum
		PCanvas->cd(6);
		gPad->SetGrid();
	//	gPad->SetLogy();				
	//	CUT = TargetCut + "&&" + TypeCut + "&&" + OutIcon;
		CUT = TargetCut + "&&" + TypeCut + "&&" + OutIcon;		
		if(tree->Draw("Energy",CUT.c_str(),"")>0)
		{
			htemp->GetXaxis()->SetLabelSize(0.05);			
			htemp->GetYaxis()->SetLabelSize(0.06);
			htemp->GetXaxis()->SetTitleSize(0.05);	
			htemp->GetYaxis()->SetTitleSize(0.05);
			htemp->SetLineWidth(2);
			col=2;			
			htemp->SetLineColor(col);
			htemp->GetXaxis()->SetTitleColor(col);
			htemp->GetYaxis()->SetTitleColor(col);			
			htemp->Clear();
		}
	//Save to a file
		string SaveName = OUTPATH + CanName + SaveType;
		PCanvas->SaveAs(SaveName.c_str());
		if(!TestFlag) 
		{
			PCanvas->Close();
			PCanvas->Clear();	
		}		
	}
	
	

//
Int_t ToDrawF(Float_t CTime,TTree *tree1,TTree *tree2)
{
	//Current Time CUT	
	stringstream Str_time;
	Str_time << CTime;
	string SCTime = Str_time.str();
	string timename = "Time==";
	string TimeCut = timename + SCTime;	
	string CanNameF = "Field-" + SCTime + "T";	
	//
	TCanvas *fCanvas = new TCanvas(CanNameF.c_str(),CanNameF.c_str(),WPixel,HPixel);
	fCanvas->Divide(3,2);
	string CUT;	
	CUT = "";
	//
	fCanvas->cd(1);
	gPad->SetGrid();	
	tree1->Draw("Ex:X",CUT.c_str(),"L");	
	Graph->SetLineColor(2);
	Graph->SetLineWidth(1);
	htemp->GetXaxis()->SetLabelSize(0.06);	
	htemp->GetYaxis()->SetLabelSize(0.06);
	htemp->GetXaxis()->SetTitleSize(0.05);	
	htemp->GetYaxis()->SetTitleSize(0.05);						
	//
	fCanvas->cd(2);
	gPad->SetGrid();			
	tree1->Draw("Ey:X",CUT.c_str(),"L");	
	Graph->SetLineColor(4);
	Graph->SetLineWidth(1);
	htemp->GetXaxis()->SetLabelSize(0.06);	
	htemp->GetYaxis()->SetLabelSize(0.06);
	htemp->GetXaxis()->SetTitleSize(0.05);	
	htemp->GetYaxis()->SetTitleSize(0.05);		
	//
	fCanvas->cd(3);
	gPad->SetGrid();			
	tree1->Draw("sqrt(Ey**2+Ez**2):X",CUT.c_str(),"L");	
	Graph->SetLineColor(4);
	Graph->SetLineWidth(1);
	htemp->GetXaxis()->SetLabelSize(0.06);	
	htemp->GetYaxis()->SetLabelSize(0.06);
	htemp->GetXaxis()->SetTitleSize(0.05);	
	htemp->GetYaxis()->SetTitleSize(0.05);			
	//all EX
	fCanvas->cd(4);		
	if(tree2->Draw("YY:XX:AEx",CUT.c_str(),"colz")>0)
	{
		htemp->GetXaxis()->SetLabelSize(0.06);	
		htemp->GetYaxis()->SetLabelSize(0.06);
		htemp->GetXaxis()->SetTitleSize(0.05);	
		htemp->GetYaxis()->SetTitleSize(0.05);
		htemp->Clear();
	}		
	//all Ey
	fCanvas->cd(5);		
	if(tree2->Draw("YY:XX:AEy",CUT.c_str(),"colz")>0)			
	{
		htemp->GetXaxis()->SetLabelSize(0.06);	
		htemp->GetYaxis()->SetLabelSize(0.06);
		htemp->GetXaxis()->SetTitleSize(0.05);	
		htemp->GetYaxis()->SetTitleSize(0.05);
		htemp->Clear();
	}		
	//all Eyz
	fCanvas->cd(6);		
	if(tree2->Draw("YY:XX:sqrt(AEz**2+AEy**2)",CUT.c_str(),"colz")>0)
	{		
		htemp->GetXaxis()->SetLabelSize(0.06);	
		htemp->GetYaxis()->SetLabelSize(0.06);
		htemp->GetXaxis()->SetTitleSize(0.05);	
		htemp->GetYaxis()->SetTitleSize(0.05);
		htemp->Clear();
	}	
	
	//Save to a eps file
	string SaveNameF = OUTPATH + CanNameF + SaveType;
	fCanvas->SaveAs(SaveNameF.c_str());
	if(!TestFlag) 
	{
		fCanvas->Close();
		fCanvas->Clear();	
	}			
}	
	
	

//
Int_t Check(Float_t CTime,TTree *tree1,TTree *tree2)
{
	//Current Time CUT	
	stringstream Str_time;
	Str_time << CTime;
	string SCTime = Str_time.str();
	string timename = "Time==";
	string TimeCut = timename + SCTime;	
	string CanNameF = "Field-" + SCTime + "T";	
	//
	TCanvas *fCanvas = new TCanvas(CanNameF.c_str(),CanNameF.c_str(),WPixel,HPixel);
	fCanvas->Divide(3,2);
	string CUT;	
	CUT = "";
	//
	fCanvas->cd(1);
	gPad->SetGrid();	
/*	tree1->Draw("Bx:X",CUT.c_str(),"L");
	
	Graph->SetLineColor(2);
	Graph->SetLineWidth(1);
	htemp->GetXaxis()->SetLabelSize(0.06);	
	htemp->GetYaxis()->SetLabelSize(0.06);
	htemp->GetXaxis()->SetTitleSize(0.05);	
	htemp->GetYaxis()->SetTitleSize(0.05);
*/
tree2->Draw("YY:XX:AEx",CUT.c_str(),"colz");						
	//
	fCanvas->cd(2);
	gPad->SetGrid();			
/*	tree1->Draw("By:X",CUT.c_str(),"L");	
	Graph->SetLineColor(4);
	Graph->SetLineWidth(1);
	htemp->GetXaxis()->SetLabelSize(0.06);	
	htemp->GetYaxis()->SetLabelSize(0.06);
	htemp->GetXaxis()->SetTitleSize(0.05);	
	htemp->GetYaxis()->SetTitleSize(0.05);		
*/
tree2->Draw("YY:XX:AEy",CUT.c_str(),"colz");
	//
	fCanvas->cd(3);
	gPad->SetGrid();			
/*	tree1->Draw("Ez:X",CUT.c_str(),"L");	
	Graph->SetLineColor(4);
	Graph->SetLineWidth(1);
	htemp->GetXaxis()->SetLabelSize(0.06);	
	htemp->GetYaxis()->SetLabelSize(0.06);
	htemp->GetXaxis()->SetTitleSize(0.05);	
	htemp->GetYaxis()->SetTitleSize(0.05);
*/
tree2->Draw("YY:XX:ABz",CUT.c_str(),"colz");			
	//all EX
	fCanvas->cd(4);		
	if(tree2->Draw("YY:XX:ABx",CUT.c_str(),"colz")>0)
	{
		htemp->GetXaxis()->SetLabelSize(0.06);	
		htemp->GetYaxis()->SetLabelSize(0.06);
		htemp->GetXaxis()->SetTitleSize(0.05);	
		htemp->GetYaxis()->SetTitleSize(0.05);
		htemp->Clear();
	}		
	//all Ey
	fCanvas->cd(5);		
	if(tree2->Draw("YY:XX:ABy",CUT.c_str(),"colz")>0)			
	{
		htemp->GetXaxis()->SetLabelSize(0.06);	
		htemp->GetYaxis()->SetLabelSize(0.06);
		htemp->GetXaxis()->SetTitleSize(0.05);	
		htemp->GetYaxis()->SetTitleSize(0.05);
		htemp->Clear();
	}		
	//all Eyz
	fCanvas->cd(6);		
	if(tree2->Draw("YY:XX:AEz",CUT.c_str(),"colz")>0)
	{		
		htemp->GetXaxis()->SetLabelSize(0.06);	
		htemp->GetYaxis()->SetLabelSize(0.06);
		htemp->GetXaxis()->SetTitleSize(0.05);	
		htemp->GetYaxis()->SetTitleSize(0.05);
		htemp->Clear();
	}	
	
	//Save to a eps file
	string SaveNameF = OUTPATH + CanNameF + SaveType;
	fCanvas->SaveAs(SaveNameF.c_str());
	if(!TestFlag) 
	{
		fCanvas->Close();
		fCanvas->Clear();	
	}			
}	
		
	

//判断文件行数
int CountLines(const char *filename)
	{
		ifstream ReadFile;
		int n=0;
		char line[512];
		ReadFile.open(filename,ios_base::in);//以只读的方式读取文件
		if(ReadFile.fail())//文件打开失败:返回0
		{
			return 0;
		}
		else//文件存在
		{
			while(!ReadFile.eof())//判断文件结尾
			{
				ReadFile.getline(line,sizeof(line));
				n++;
			}
	    return n-1;//把多数的一行减掉
	    }
    ReadFile.close();
    }		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
