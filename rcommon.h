!	rpic.f90源代码的头文件rcommon.h
!
!定义一些常量，mc12：碳12质量，trans:从J到eV的转换系数
real*8 pi,co,miu,ebsn,q,me,mc12,trans,mebsne2
parameter(pi=3.1415927d0,co=3d8,miu=1.26d-6,ebsn=8.85d-12,&
          q=-1.6d-19,me=9.11d-31,mc12=1.667d-27,trans=1.602d-19,mebsne2=me*ebsn/q**2)
!===========================================================
integer tgt1,tgt2,tgtin,ck,teonf,tmonf,ijnum,ijnumo,totstep
integer xnum,ynum,filetype,onoroff,pois_init,pois_onf,pois_fix,pois_type
integer pboundlr,pboundud,lrbd,udbd
real*8 lx,ly,totimet,totime,ctx,cty,dx,dy,dt,s,btime,ftime,gtime,movetime
common/init/totime,totstep
parameter(             &
lx=10d-6              ,&!模拟区域宽度
ly=20d-6              ,&!模拟区域高度
totimet=20d0          ,&!模拟时间/周期
xnum=1000              ,&!X方向格子数
ynum=1000             ,&!Y方向格子数
dx=lx/dble(xnum)	  ,&!单个格子宽度
dy=ly/dble(ynum)      ,&!单个格子高度
ctx=0.5d0             ,&!c*dt/dx，用FDTD解MAXWEL时用的参数
movetime=5            ,&!开始计算粒子运动的时间/周期
tgt1=1                ,&!是否用靶1，用（1），不用（0）
tgt2=0                ,&!是否用靶2，用（1），不用（0）
teonf=1               ,&!TE模开关，开（1），关（0），解Ex,Ey,Bz
tmonf=1               ,&!TM模开关，开（1），关（0），解Bx,By,Ez
btime=0               ,&!开始记录数据的时刻/周期
ftime=totimet         ,&!结束记录数据的时刻/周期
gtime=1               ,&!记录数据的时间间隔/周期
ck=(ftime-btime)/gtime,&!输出数据次数，间隔一定时间就输出一次数据
pboundlr=1            ,&!粒子左右边界条件,0:周期,1:吸收
pboundud=0            ,&!粒子上下边界条件,0:周期,1:吸收
lrbd=1                ,&!电磁场左右边界条件，1：Mur1,2:Mur2
udbd=1                ,&!电磁场上下边界条件，0：周期，1：Mur1,2:Mur2
pois_init=1 	      ,&!初始化时是否poisson修正,0:不修正,1:修正
pois_onf=1	          ,&!计算过程中是否poisson修正,0:不修正,1:修正
pois_fix=100          ,&!每多少个dt进行一次poisson修正
pois_type=1           ,&!poisson修正的左右边界条件，0：电势为0，1：开放边界
filetype=0            ,&!输出文件类型，0：ROOT，1：TXT，2：BOTH
dt=ctx*dx/co          ,&!时间步长
cty=co*dt/dy          ,&!c*dt/dy
s=dx*dy               ,&!单个计算格子面积
ijnumo=xnum*ynum      ,&!总的格个数
ijnum=(xnum+1)*(ynum+1)&!总的格点个数
)
!===========================================================
!	格点参数
integer celli1,celle1,celli2,celle2,celli3
parameter(             &
celli1=10             ,&!靶1每格离子个数
celle1=10             ,&!靶1每格电子个数
celli2=3             ,&!靶2每格离子个数
celle2=3              &!靶-每格电子个数
)
!ID,DO NOT DELETE
integer snumi1,snume1,snumi2,snume2,snumi3
parameter(             &
  snumi1=        1,&!靶-1 离子个数
  snume1=        1,&!靶-1 电子个数
  snumi2=        1,&!靶-2 离子个数
  snume2=        1 &!靶-2 电子个数
)

!===========================================================
!	激光参数
!===========================================================
integer gs1,polar1,side1,udshape1
real*8 pstart1,posit1,a1,phase1,r1,lamda1,tup1,tdur1,tdown1,per1,womiga1,p1t1,p1t2,p1t3,pfin1
real*8 ptop1,ptao1,womiga,per
integer gs2,polar2,side2,udshape2
real*8 pstart2,posit2,a2,phase2,r2,lamda2,tup2,tdur2,tdown2,per2,womiga2,p2t1,p2t2,p2t3,pfin2
real*8 ptop2,ptao2
integer pnum
common/pulse1/p1t1,p1t2,p1t3,pfin1
common/pulse2/p2t1,p2t2,p2t3,pfin2

parameter(		pnum=1    )!脉冲个数
!=================Pulse1=================
parameter(          &
pstart1=0			,&! 脉冲起始时间/周期
posit1=0.5d0		,&!中心坐标
side1=0           ,&!0:left,1:right
a1=100d0           ,&!振幅
polar1=0           ,&!偏振方式，圆偏振(0),P极化(1),S极化(2)
phase1=0d0			,&!相位调制，单位:PI
gs1=2              ,&!Y方向高斯分布形状因子
lamda1=1d-6        ,&!波长
r1=3*lamda1       ,&!焦斑半径
per1=lamda1/co       ,&!计算单个激光脉冲的周期
per=per1       ,&!计算激光脉冲的周期
womiga1=2.*pi/per1	   ,&!当前脉冲的频率
womiga=womiga1	   ,&!当前脉冲的频率
udshape1=2			,&!当前脉冲的形状，0：sin**0.5()，
								   !1:sin()，
								   !2:liner(),
								   !3:exp(-((t-to)/tao)**2)
!!!!!!!!!!!!sin()&sin**2()&liner()!!!!!!!
tup1=1d0             ,&!上升时间，周期数
tdur1=8d0            ,&!持续时间，周期数
tdown1=1d0           ,&!下降时间，周期数
!!!!!!!!!!!!exp()!!!!!!!
ptop1=50d0             ,&!高斯顶点出现时间，周期数
ptao1=25d0             &!高斯的sigma，周期数
)
real*8 nc
parameter(   nc=mebsne2*(2d0*pi*co/lamda1)**2 )
!=================Pulse2=================
parameter(          &
pstart2=0		,&! 脉冲起始时间/周期
posit2=0.5		,&!中心坐标
side2=1           ,&!0:left,1:right
a2=60d0           ,&!振幅
polar2=0           ,&!偏振方式，圆偏振(0),P极化(1),S极化(2)
phase2=0			,&!相位调制，单位:PI
gs2=4              ,&!Y方向高斯分布形状因子
lamda2=1e-6        ,&!波长
r2=10*lamda2       ,&!焦斑半径
per2=lamda2/co       ,&!计算单个激光脉冲的周期
womiga2=2.*pi/per2	   ,&!当前脉冲的频率
udshape2=3			,&!当前脉冲的形状，0：sin()，
								   !1:sin**2()，
								   !2:liner(),
								   !3:exp(-((t-to)/tao)**2)
!!!!!!!!!!!!sin()&sin**2()&liner()!!!!!!!
tup2=3d0             ,&!上升时间，周期数
tdur2=3d0            ,&!持续时间，周期数
tdown2=3d0           ,&!下降时间，周期数
!!!!!!!!!!!!exp()!!!!!!!
ptop2=40d0             ,&!高斯顶点出现时间，周期数
ptao2=16.18d0             &!高斯的sigma，周期数
)

!===========================================================
!	靶1 参数
!===========================================================
!==============靶1====================
integer exte1,initi1,inite1
real*8 tgtx1,tgty1,tgtmx1,tgtmy1,edens1,relam1
parameter(           &
relam1=1d0           ,&!相对原子质量
exte1=1          ,&!核外电子数
edens1=320*nc       ,&!电子密度
initi1=0.0d0             ,&!离子初始温度,Kev
inite1=0.0d0             ,&!电子初始温度,Kev
tgtx1=0.1d-6        ,&!宽度
tgty1=8d-6         ,&!高度
tgtmx1=5d-6         ,&!左边中心横坐标
tgtmy1=ly*0.5d0         &!左边中心纵坐标ly*0.5d0
)
!靶表面的孔和密度梯度参数
integer honum1,grad1,grshpf1,grshpr1
real*8  grsta1,grend1
parameter(           &
honum1=0            ,&!孔的数量
grad1=0             ,&!表面梯度，均匀（0）,前表面梯度(1),后表面梯度(2),前后表面梯度(3)
grshpf1=1           ,&!前表面梯度形式,线性(0),高斯(1),正弦(2)
grshpr1=2           ,&!后表面梯度形式,线性(0),高斯(1),正弦(2)
grsta1=0.5          ,&!前表面梯度的宽度占靶宽度的比列
grend1=0.5           &!后表面梯度的宽度占靶宽度的比列
)
!=========靶1的孔================
! Hole-1
integer hoshp1,hogs1,honf1
real*8 hox1,hoy1,hosiz1,howid1
integer horf11,horf12,horf13
real*8 hocp1x1,hocp1y1,hodg11,hocp1x2,hocp1y2,hodg12
parameter(           &
hoshp1=2            ,&!圆（0）,左开口高斯（1）,右开口高斯（2），线性规划（3）
honf1=1             ,&!规划方位内的点，舍弃（0）或保留（1）
hogs1=2             ,&!高斯形状因子
hox1=5.15e-6          ,&!中心位置横坐标
hoy1=16e-6           ,&!中心位置纵坐标
hosiz1=0.15e-6         ,&!半径或高斯深度
howid1=7e-6          ,&!高斯半高宽
hocp1x1=20e-6         ,&!第一个交点的横坐标
hocp1y1=16.5e-6         ,&!第一个交点的纵坐标
hodg11=167           ,&!第一条直线的倾斜度数
hocp1x2=20e-6         ,&!第二个交点的横坐标
hocp1y2=13.5e-6         ,&!第二个交点的纵坐标
hodg12=13           ,&!第二条直线的倾斜度数，第三条直线方程由两个交点确定
horf11=0           ,&!第一条直线的左或者右边
horf12=1           ,&!第二条直线的左或者右边
horf13=0            &!第三条直线的左或者右边
)
! Hole-2
integer hoshp2,hogs2,honf2
real*8 hox2,hoy2,hosiz2,howid2
integer horf21,horf22,horf23
real*8 hocp2x1,hocp2y1,hodg21,hocp2x2,hocp2y2,hodg22
parameter(           &
hoshp2=3            ,&!圆（0）,左开口高斯（1）,右开口高斯（2），线性规划（3）
honf2=1             ,&!规划方位内的点，舍弃（0）或保留（1）
hogs2=2             ,&!高斯形状因子
hox2=1.5e-6          ,&!中心位置横坐标
hoy2=5d-6          ,&!中心位置纵坐标
hosiz2=5e-6         ,&!半径或高斯深度
howid2=2e-6          ,&!高斯半高宽
hocp2x1=22e-6         ,&!第一个交点的横坐标
hocp2y1=18.5e-6         ,&!第一个交点的纵坐标
hodg21=167           ,&!第一条直线的倾斜度数
hocp2x2=22e-6         ,&!第二个交点的横坐标
hocp2y2=11.5e-6         ,&!第二个交点的纵坐标
hodg22=13           ,&!第二条直线的倾斜度数，第三条直线方程由两个交点确定
horf21=0           ,&!第一条直线的左或者右边
horf22=1           ,&!第二条直线的左或者右边
horf23=0            &!第三条直线的左或者右边
)
! Hole-3
integer hoshp3,hogs3,honf3
real*8 hox3,hoy3,hosiz3,howid3
integer horf31,horf32,horf33
real*8 hocp3x1,hocp3y1,hodg31,hocp3x2,hocp3y2,hodg32
parameter(           &
hoshp3=0            ,&!圆（0）,左开口高斯（1）,右开口高斯（2），线性规划（3）
honf3=0             ,&!规划方位内的点，舍弃（0）或保留（1）
hogs3=2             ,&!高斯形状因子
hox3=21e-6          ,&!中心位置横坐标
hoy3=15e-6          ,&!中心位置纵坐标
hosiz3=1e-6         ,&!半径或高斯深度
howid3=5e-6        ,  &!高斯半高宽
hocp3x1=7e-6         ,&!第一个交点的横坐标
hocp3y1=7e-6         ,&!第一个交点的纵坐标
hodg31=160           ,&!第一条直线的倾斜度数
hocp3x2=7e-6         ,&!第二个交点的横坐标
hocp3y2=3e-6         ,&!第二个交点的纵坐标
hodg32=20           ,&!第二条直线的倾斜度数，第三条直线方程由两个交点确定
horf31=0           ,&!第一条直线的左或者右边
horf32=0           ,&!第二条直线的左或者右边
horf33=0            &!第三条直线的左或者右边
)
!===========================================================
!	靶2 参数
!===========================================================
!==============靶 2====================
integer relam2,exte2,initi2,inite2
real*8 tgtx2,tgty2,tgtmx2,tgtmy2,edens2
parameter(           &
relam2=12            ,&!相对原子质量
exte2=6             ,&!核外电子数
edens2=320*nc      ,&!电子密度
initi2=0            ,&!离子初始温度,Kev
inite2=0            ,&!电子初始温度,Kev
tgtx2=0.066d-6       ,&!宽度
tgty2=32d-6         ,&!高度
tgtmx2=10.033d-6     ,&!左边中心横坐标
tgtmy2=ly*0.5         &!左边中心纵坐标
)
!holes and gradient of surface
integer honum2,grad2,grshpf2,grshpr2
real*8  grsta2,grend2
parameter(           &
honum2=0            ,&!孔的数量
grad2=0             ,&!表面梯度，均匀（0）,前表面梯度(1),后表面梯度(2),前后表面梯度(3)
grshpf2=1           ,&!前表面梯度形式,线性(0),高斯(1),正弦(2)
grshpr2=2           ,&!后表面梯度形式,线性(0),高斯(1),正弦(2)
grsta2=0.5          ,&!前表面梯度的宽度占靶宽度的比列
grend2=0.5           &!后表面梯度的宽度占靶宽度的比列
)

!=========Holes in Target-2================
! Hole-4
integer hoshp4,hogs4,honf4
real*8 hox4,hoy4,hosiz4,howid4
integer horf41,horf42,horf43
real*8 hocp4x1,hocp4y1,hodg41,hocp4x2,hocp4y2,hodg42
parameter(           &
hoshp4=0            ,&!圆（0）,左开口高斯（1）,右开口高斯（2），线性规划（3）
honf4=1             ,&!规划方位内的点，舍弃（0）或保留（1）
hogs4=2             ,&!高斯形状因子
hox4=12e-6          ,&!中心位置横坐标
hoy4=16e-6          ,&!中心位置纵坐标
hosiz4=3e-6         ,&!半径或高斯深度
howid4=3e-6         ,&!高斯半高宽
hocp4x1=7e-6         ,&!第一个交点的横坐标
hocp4y1=7e-6         ,&!第一个交点的纵坐标
hodg41=160           ,&!第一条直线的倾斜度数
hocp4x2=7e-6         ,&!第二个交点的横坐标
hocp4y2=3e-6         ,&!第二个交点的纵坐标
hodg42=20           ,&!第二条直线的倾斜度数，第三条直线方程由两个交点确定
horf41=0           ,&!第一条直线的左或者右边
horf42=0           ,&!第二条直线的左或者右边
horf43=0            &!第三条直线的左或者右边
)
! Hole-5
integer hoshp5,hogs5,honf5
real*8 hox5,hoy5,hosiz5,howid5
integer horf51,horf52,horf53
real*8 hocp5x1,hocp5y1,hodg51,hocp5x2,hocp5y2,hodg52
parameter(           &
hoshp5=1            ,&!圆（0）,左开口高斯（1）,右开口高斯（2），线性规划（3）
honf5=1             ,&!规划方位内的点，舍弃（0）或保留（1）
hogs5=4             ,&!高斯形状因子
hox5=12e-6          ,&!中心位置横坐标
hoy5=7e-6          ,&!中心位置纵坐标
hosiz5=2e-6         ,&!半径或高斯深度
howid5=2e-6          ,&!高斯半高宽
hocp5x1=7e-6         ,&!第一个交点的横坐标
hocp5y1=7e-6         ,&!第一个交点的纵坐标
hodg51=160           ,&!第一条直线的倾斜度数
hocp5x2=7e-6         ,&!第二个交点的横坐标
hocp5y2=3e-6         ,&!第二个交点的纵坐标
hodg52=20           ,&!第二条直线的倾斜度数，第三条直线方程由两个交点确定
horf51=0           ,&!第一条直线的左或者右边
horf52=0           ,&!第二条直线的左或者右边
horf53=0            &!第三条直线的左或者右边
)
! Hole-6
integer hoshp6,hogs6,honf6
real*8 hox6,hoy6,hosiz6,howid6
integer horf61,horf62,horf63
real*8 hocp6x1,hocp6y1,hodg61,hocp6x2,hocp6y2,hodg62
parameter(           &
hoshp6=2            ,&!圆（0）,左开口高斯（1）,右开口高斯（2），线性规划（3）
honf6=1             ,&!规划方位内的点，舍弃（0）或保留（1）
hogs6=2             ,&!高斯形状因子
hox6=18e-6          ,&!中心位置横坐标
hoy6=13e-6          ,&!中心位置纵坐标
hosiz6=3e-6         ,&!半径或高斯深度
howid6=1e-6          ,&!高斯半高宽
hocp6x1=7e-6         ,&!第一个交点的横坐标
hocp6y1=7e-6         ,&!第一个交点的纵坐标
hodg61=160           ,&!第一条直线的倾斜度数
hocp6x2=7e-6         ,&!第二个交点的横坐标
hocp6y2=3e-6         ,&!第二个交点的纵坐标
hodg62=20           ,&!第二条直线的倾斜度数，第三条直线方程由两个交点确定
horf61=0           ,&!第一条直线的左或者右边
horf62=0           ,&!第二条直线的左或者右边
horf63=0            &!第三条直线的左或者右边
)

!===========================================================
!=========一些全局变量=========================
!===========================================================
!===========================================================


!===========================================================
!laser common
!===========================================================
real*8 laserao(6,xnum)
real*8 lari1,lari2
integer polarsign,laserl,laserr
common/laser1/laserao
common/laser2/lari1,lari2
common/laser3/polarsign,laserl,laserr

!===========================================================
!phy common
!===========================================================
integer step,cki,pipe,rcdsteps,bstep,fstep,poutnum
character*3 outname
real*8 pois_error,fdtrans,fdtransb
real*8 rtime,det,totfield,totpart,totlaser,timeout
real*8 cput1,cput2,cput3,cput4,cput5,cput0,cput10
real*8 cputp1,cputp2,cputp3,cputp4,cputp5,cputp12,cputp23,cputp34,cputp45
real*8 cputp6,cputp7,cputp8,cputp9,cputp10,cputp67,cputp78,cputp89,cputp90
common/poisson/pois_error,fdtrans,fdtransb
common/phy1/rtime,det,totfield,totpart,totlaser,timeout
common/phy2/step,cki,pipe,outname
common/phy3/cput1,cput2,cput3,cput0,cput10
common/phy4/rcdsteps,bstep,fstep,poutnum
common/phy5/cputp1,cputp2,cputp3,cputp4,cputp5
common/phy50/cputp12,cputp23,cputp34,cputp45
common/phy6/cputp6,cputp7,cputp8,cputp9,cputp10
common/phy60/cputp67,cputp78,cputp89,cputp90
!===========================================================
!maxwel common
!===========================================================
real*8 tpx,tpy,tpex,tpey,coo,ctx1,ctx2,cty1,cty2,dxy,jd1,jd2,jd3,jd4
real*8 fparx,fpary,hparx,hpary
real*8 tempbzl(ynum),tempbzr(ynum),tempbzd(xnum),tempbzu(xnum)
real*8 tempezl(ynum+1),tempezr(ynum+1),tempezd(xnum+1),tempezu(xnum+1)
common/phy5/tpx,tpy,tpex,tpey,coo,ctx1,ctx2,cty1,cty2,dxy,jd1,jd2,jd3,jd4
common/phy6/tempbzl,tempbzr,tempbzd,tempbzu,tempezl,tempezr,tempezd,tempezu
common/phy7/fparx,fpary,hparx,hpary

!===========================================================
!grid common，格点上的一些参量
!===========================================================
!半格点上的X、Y、Z方向的电场，注意数组的尺寸和量得个数是一一对应的
real*8 fdex(xnum,ynum+1),fdey(xnum+1,ynum),fdbz(xnum,ynum)
!整格点上的X、Y、Z方向的电场
real*8 fdex1(xnum+1,ynum+1),fdey1(xnum+1,ynum+1),fdbz1(xnum+1,ynum+1)
!半格点上的X、Y、Z方向的磁场
real*8 fdbx(xnum+1,ynum),fdby(xnum,ynum+1),fdez(xnum+1,ynum+1)
!整格点上的X、Y、Z方向的磁场，这里没有fdez1，是因为Ez本身就在整格点上
real*8 fdbx1(xnum+1,ynum+1),fdby1(xnum+1,ynum+1)
!把这些量定义成全局变量
common/fielda/fdex,fdey,fdbz
common/fieldb/fdex1,fdey1,fdbz1
common/fieldc/fdbx,fdby,fdez
common/fieldd/fdbx1,fdby1

!===========================================================
!particles common
!===========================================================
real*8 cutx(xnum,ynum+1),cuty(xnum+1,ynum),cutz(xnum+1,ynum+1),row(xnum+1,ynum+1)
common/cut/cutx,cuty,cutz,row

integer cnumi1,piji1(3,snumi1),piji1b(3,snumi1),iconi1(snumi1)
real*8 xyi1(3,snumi1),xyi1b(3,snumi1),weiti1(snumi1),cmi1(snumi1),cqi1(snumi1)
real*8 vxyi1(4,snumi1),pxyi1(4,snumi1),jxi1(snumi1),jyi1(snumi1),jzi1(snumi1)
real*8 wti1,idens1,mi1,qi1,realnum1i,engi1(snumi1),gamai1(snumi1),angi1(snumi1),disti1(2,snumi1)
real*8 cutxi1(xnum,ynum+1),cutyi1(xnum+1,ynum),cutzi1(xnum+1,ynum+1),rowi1(xnum+1,ynum+1)
common/parti1a/cnumi1,piji1,piji1b,iconi1
common/parti1b/xyi1,xyi1b,weiti1,cmi1,cqi1
common/parti1c/vxyi1,pxyi1,jxi1,jyi1,jzi1
common/parti1d/wti1,idens1,mi1,qi1,realnum1i,engi1,gamai1,angi1,disti1
common/parti1e/cutxi1,cutyi1,cutzi1,rowi1

integer cnume1,pije1(3,snume1),pije1b(3,snume1),icone1(snume1)
real*8 xye1(3,snume1),xye1b(3,snume1),weite1(snume1),cme1(snume1),cqe1(snume1)
real*8 vxye1(4,snume1),pxye1(4,snume1),jxe1(snume1),jye1(snume1),jze1(snume1)
real*8 wte1,realnum1e,enge1(snume1),gamae1(snume1),ange1(snume1),diste1(2,snume1)
real*8 cutxe1(xnum,ynum+1),cutye1(xnum+1,ynum),cutze1(xnum+1,ynum+1),rowe1(xnum+1,ynum+1)
common/parte1a/cnume1,pije1,pije1b,icone1
common/parte1b/xye1,xye1b,weite1,cme1,cqe1
common/parte1c/vxye1,pxye1,jxe1,jye1,jze1
common/parte1d/wte1,realnum1e,enge1,gamae1,ange1,diste1
common/parte1e/cutxe1,cutye1,cutze1,rowe1

integer cnumi2,piji2(3,snumi2),piji2b(3,snumi2),iconi2(snumi2)
real*8 xyi2(3,snumi2),xyi2b(3,snumi2),weiti2(snumi2),cmi2(snumi2),cqi2(snumi2)
real*8 vxyi2(4,snumi2),pxyi2(4,snumi2),jxi2(snumi2),jyi2(snumi2),jzi2(snumi2)
real*8 wti2,idens2,mi2,qi2,realnum2i,engi2(snumi2),gamai2(snumi2),angi2(snumi2),disti2(2,snumi2)
real*8 cutxi2(xnum,ynum+1),cutyi2(xnum+1,ynum),cutzi2(xnum+1,ynum+1),rowi2(xnum+1,ynum+1)
common/parti2a/cnumi2,piji2,piji2b,iconi2
common/parti2b/xyi2,xyi2b,weiti2,cmi2,cqi2
common/parti2c/vxyi2,pxyi2,jxi2,jyi2,jzi2
common/parti2d/wti2,idens2,mi2,qi2,realnum2i,engi2,gamai2,angi2,disti2
common/parti2e/cutxi2,cutyi2,cutzi2,rowi2

integer cnume2,pije2(3,snume2),pije2b(3,snume2),icone2(snume2)
real*8 xye2(3,snume2),xye2b(3,snume2),weite2(snume2),cme2(snume2),cqe2(snume2)
real*8 vxye2(4,snume2),pxye2(4,snume2),jxe2(snume2),jye2(snume2),jze2(snume2)
real*8 wte2,realnum2e,enge2(snume2),gamae2(snume2),ange2(snume2),diste2(2,snume2)
real*8 cutxe2(xnum,ynum+1),cutye2(xnum+1,ynum),cutze2(xnum+1,ynum+1),rowe2(xnum+1,ynum+1)
common/parte2a/cnume2,pije2,pije2b,icone2
common/parte2b/xye2,xye2b,weite2,cme2,cqe2
common/parte2c/vxye2,pxye2,jxe2,jye2,jze2
common/parte2d/wte2,realnum2e,enge2,gamae2,ange2,diste2
common/parte2e/cutxe2,cutye2,cutze2,rowe2






